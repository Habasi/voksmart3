<?php
#region Базовые функции
/**
 * Вывод отладочной информации
 * @param $data
 * @param string $name
 */
function dump($data, $name = ''){
    $buf = str_replace('\\r', '', var_export($data, true));
    $buf = preg_replace('/\(array\(/s', '', $buf);
    $buf = preg_replace('/\)\)/s', '', $buf);
    $buf = preg_replace('/stdClass::__set_state/s', '', $buf);
    
    if ($name) echo $name, '=';
    
    echo 
    '<div class="modal dump-modal fade show" tabindex="-1" role="dialog" style="padding-right: 17px; display: block; background: #00000087;">'.
    '  <div class="modal-dialog" role="document">'.
    '    <div class="modal-content">'.
    '      <div class="modal-header">'.
    '        <h5 class="modal-title" >Отладка</h5>'.
    '        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="this.closest(\'.dump-modal\').remove();">'.
    '          <span aria-hidden="true">×</span>'.
    '        </button>'.
    '      </div>'.
    '      <div class="modal-body" style="max-height:80vh; overflow-y:auto;">'.
    '        <p><pre>'.$buf.'</pre></p>'.
    '      </div>'.
    '    </div>'.
    '  </div>'.
    '</div>';
}

function tpl($name) {
    require_once DIR . "/docroot/" . $name;
}

function html($name) {
    echo file_get_contents(DIR . $name);
}
function getJson($name){
    // dump($_SESSION);
    // if($_SESSION['db-json']){
    // } else {
        $json = file_get_contents(DIR . "/docroot/json/".$name.".json");
        $json = json_decode($json, true);
        // $_SESSION['db-json'] = $json;
    // }
    return $json;
}
#endregion

function pageHead($title = "Страница") {return;
    require_once DIR . "/docroot/includes/head.php";
}

// Распарсить $_POST
function getPost(){
    $arguments = $_POST['arguments'];
    if($arguments == '{}') return null;
    if($arguments) return json_decode($arguments);
    else return null;
}

// файл с настройками // после загрузки хранится в сессии
function getDBJson(){
    // dump($_SESSION);
    // if($_SESSION['db-json']){
    // } else {
        $json = @file_get_contents(DIR . "/docroot/db.json");
        $json = json_decode($json, true);
        $_SESSION['db-json'] = $json;
    // }
    return $_SESSION['db-json'];
}
// файл с настройками // после загрузки хранится в сессии
function getDBJsonForJS(){
    echo json_encode(getDBJson());
}

// получить имя текущего скина
function qurrentSkin(){
    $json = getDBJson();
    if($_SESSION['current-skin']){
        // return $_SESSION['current-page'];
    } else {
        $_SESSION['current-skin'] = $json['current-skin'];
    }
    return $_SESSION['current-skin'];
}

function changeSkin($skin_name){
    $_SESSION['current-skin'] = $skin_name;
}

// получить имя текущего скина
function qurrentPage(){
    $json = getDBJson();
    if($_SESSION['current-page']){
        // return $_SESSION['current-page'];
    } else {
        $_SESSION['current-page'] = $json['current-page'];
    }
    return $_SESSION['current-page'];
}

// получить инфо о скине
function getSkinInfo($skin){
    $json = getDBJson();
    $item = null;
    foreach($json['skin'] as $struct) {
        if ($struct['name'] == $skin) {
            $item = $struct;
            break;
        }
    };

    $item['count_widgets'] = count(scandir(DIR . "/docroot/skin/".$item['name'].'/include')) -2;
    // dump($count_widgets);

    return $item;
}

function getPageInfo($page_name){
    $page_data = getPage($page_name);
    return $page_data;
}

// вернет верхнее меню в виде json строки
function getJsonMenu(){
    $json = getDBJson();
    echo json_encode($json["menu"]);
}

function includeCSS($skin_name){
    if(!$skin_name) $skin_name = qurrentSkin();
    $skin = getSkinInfo($skin_name);
    $path = $skin["path"];

    foreach ($skin["css"] as $value) {
        echo '<link rel="stylesheet" href="'./*$path.*/$value.'">';
    }
}

function includeJS($skin_name){
    if(!$skin_name) $skin_name = qurrentSkin();
    $skin = getSkinInfo($skin_name);
    $path = $skin["path"];

    foreach ($skin["js"] as $value) {
        echo '<script src="'./*$path.*/$value.'"></script>';
    }
}

function playPage($page_name, $is_preview){
    $skin = getSkinInfo(qurrentSkin());
    $page = getPage($page_name);
    require_once(DIR . "/docroot/index.php");
}

function playSkin($skin, $page, $is_preview, $blok_name){
    $title = $page->name;
    require_once(DIR . "/docroot/skin/".$skin['name']."/include/template.php");
}

function includepageBlocks($skin, $page){
    foreach($page->rows as $struct) {
        includeBlock($skin['name'], $struct->name);
    };
}

function includeBlock($skin_name, $block_name){
    if(!$skin_name) $skin_name = qurrentSkin();
    $skin = getSkinInfo($skin_name);
    $path = $skin["path"];

    echo '<section class="widget" name="'.$block_name.'"></section>';
}

// подготовить объект с заполненными текстами для блока ({ mail:'fswdf', phone:'sdff', ... }) по имени блока
function prepareBlockTexts($block_name){
    $texts_to_return = [];
    $block = getWidget($block_name);
    // dump($block);
    $texts = getTextsList()->texts;

    if(!$block || !$block->texts) return $texts_to_return;
    // Заполнить тексты
    foreach($block->texts as $key => $t) {
        $txt = null;
        foreach($texts as $struct) {
            if ($struct->id == $t) {
                $txt = $struct->text;
                break;
            }
        };
        
        $texts_to_return[$key] = $txt;
    };
    // dump($texts_to_return);
    
    return $texts_to_return;
}

// отобразить текст из заготовок на блоке
function apchi($name_text){
    $texts = getJson('data')["text"];
    $txt = $texts[$name_text] ? $texts[$name_text]["text"] : '[текст]';
    $txt = '<span class="edit" id="text-'.$name_text.'">'.$txt.'<i class="fa fa-pen" onclick="edit(\\\''.$name_text.'\\\');"></i></span>';

    echo $txt;
}
// отобразить картинку
function image($blok_name, $name_img, $preview = false){
    $block = getWidget($blok_name);
    if($block){
        $img = $block->images->$name_img ?: '';
        echo '/docroot/images/'.$img;
    }
}

function getFaviconURL(){
    return '/docroot/src/img/favicon.ico';
}

function updateFavicon(){
    return `/docroot/src/img/favicon.ico`;
}

function getIncludesBlockValues($block, $name_list){
    $list = null;
    foreach($block->includes as $key => $value) if($value->name == $name_list) $list = $value;
    return $list->values;
}

function ingItToUrl($id){
    $image = getImage($id);
    return $image ? $image->image : '';
}


function base64_to_savedImage($base64_string, $id) {
    $type = explode('/', mime_content_type($base64_string))[1];
    // open the output file for writing
    $ifp = fopen( DIR."/docroot/images/".$id.'.'.$type, 'wb' );
    // split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode( ',', $base64_string );
    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $data[ 1 ] ) );
    // clean up the file resource
    fclose( $ifp );
}

/**
 * Получить base64 из ID
 * getImageFromID('test-image');
 */
function getImageFromID($id){
    // Получить полное имя файла с расширением
    $images_names_arr = scandir(DIR."/docroot/images/");
    $image_name = null;
    foreach($images_names_arr as $key => $value){
        $t = explode( '.', $value );
        if($t[0] == $id){
            $image_name = $value;
        }
    }

    $path = DIR."/docroot/images/".$image_name;
    $type = pathinfo($path, PATHINFO_EXTENSION);

    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
    return $base64;
}
