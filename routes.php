<?
Router::route('/', function () {
        require_once(DIR . "/docroot/index.php");
});
Router::route('/supply', function () {
        require_once(DIR . "/docroot/supply.php");
});
Router::route('/projects', function () {
        require_once(DIR . "/docroot/projects.php");
});
Router::route('/certificat', function () {
        require_once(DIR . "/docroot/certificat.php");
});
Router::route('/montage', function () {
        require_once(DIR . "/docroot/montage.php");
});
Router::route('/contact', function () {
        require_once(DIR . "/docroot/contact.php");
});
Router::route('/contact', function () {
        require_once(DIR . "/docroot/contact.php");
});
Router::route('404', function () {
        require_once(DIR . "/docroot/pages-404.html");
});

Router::route('/get-data', function ($wz) {
        $json = file_get_contents(DIR . "/docroot/json/".'data'.".json");
        echo $json ?: '{}';
});
Router::route('/set-data', function ($wz) {
        $data = getPost();
        $r = file_put_contents(DIR . "/docroot/json/".'data'.".json", $_POST['arguments']);
        echo json_encode($data);
});
