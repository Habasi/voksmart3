<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Воксмарт</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <? require_once(DIR . "/docroot/include/styles.php"); ?>
    
  </head>
  <body>
    <? require_once(DIR . "/docroot/include/header.php"); ?>
	
    <? require_once(DIR . "/docroot/include/home-slider.php"); ?>
    <? require_once(DIR . "/docroot/include/numbers.php"); ?>
    <? require_once(DIR . "/docroot/include/services.php"); ?>
    <? require_once(DIR . "/docroot/include/intro.php"); ?>
    <? require_once(DIR . "/docroot/include/scheme.php"); ?>
    <? require_once(DIR . "/docroot/include/projects.php"); ?>
    <? //require_once(DIR . "/docroot/include/team.php"); ?>
    <? //require_once(DIR . "/docroot/include/recent-blog.php"); ?>
    <? require_once(DIR . "/docroot/include/appintment.php"); ?>
		
  	<? require_once(DIR . "/docroot/include/footer.php"); ?>
  	<? require_once(DIR . "/docroot/include/loader.php"); ?>
    <? require_once(DIR . "/docroot/include/scripts.php"); ?>
    
  </body>
</html>