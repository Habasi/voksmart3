<!DOCTYPE html>
<html lang="en">

<head>
  <title>Воксмарт - Сертификаты</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <? require_once(DIR . "/docroot/include/styles.php"); ?>

</head>

<body>
  <? require_once(DIR . "/docroot/include/header.php"); ?>

  <div class="swiper">
    <i>
      <input checked type="radio" name="sertificate" style="background-image: url('/docroot/images/sertificate1.jpg');" title="Сертификат 1">
      <input type="radio" name="sertificate" style="background-image: url('/docroot/images/sertificate2.jpg');" title="Сертификат 2">
    </i>
  </div>

  <? require_once(DIR . "/docroot/include/footer.php"); ?>
  <? require_once(DIR . "/docroot/include/loader.php"); ?>
  <? require_once(DIR . "/docroot/include/scripts.php"); ?>

</body>

</html>