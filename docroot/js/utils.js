var vers = false; //результат проверки версии

function edit(name){
    let text = getJson('/get-data');
    text = text.text[name]
    if(!text) return false;
    // console.log(text);

    let modal = document.createElement('dialog');
    modal.style.width = '500px';
    let id = 'modal' + createUUID();
    modal.id = id;
    modal.innerHTML = `
        <h2>${'Изменить текст'}</h2>
        <p>
            <textarea style="width:100%; height:300px;">${text.text}</textarea><br>
            <label></label><br>
            <button class="save-text">Сохранить</button>
            <footer class="blockquote-footer">${name}</footer>
        </p>
        <button aria-label="close" class="x">❌</button>
    `;
    document.body.append(modal);
    window[id].showModal();
    modal.querySelector('.x').onclick = function(){
        window[id].close();
        modal.remove();
    }
    modal.querySelector('.save-text').onclick = function(){
        let new_text = modal.querySelector('textarea').value;
        let text2 = getJson('/get-data');
        text2.text[name].text = new_text;
        console.log(text2);
        post('/set-data', text2, function(a){
            // console.log(a);
        });
        modal.querySelector('.x').onclick();
        console.log(document.querySelector(`#text-${name}`), new_text);
        let i = document.querySelector(`#text-${name} i.fa`);
        document.querySelectorAll(`#text-${name}`).forEach( c => c.innerHTML = new_text + i.outerHTML );
    }
}

/**
 * Получить данные любого типа по апи
 * @author Nelly
 * @param {string}   url    - Апи
 * @param {function} fname  - Функция обработки полученных данных
 * @param {string}   method - Метод
 * Пример использования:
 *   getAny('/route.html', function(data){
 *   }, 'GET');
 */
function getAny(url, fname, method) {
    method = method ? method : 'GET';
    
    var xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    xhr.send();
    
    xhr.onreadystatechange = function(e) {
        // Ошибка
        if (xhr.readyState != 4) return false;
            
        // Ошибка
        if (xhr.status != 200 && xhr.status != 501) {
            if (Number(xhr.status) == 0) error_txt = "Отсутствует связь с сервером! ";
            else error_txt = "Ошибка обращения к серверу";
            setError(error_txt, xhr.status, 'getDataError');
            // Ошибка
        } else if (xhr.status == 501) {
            window.document.location.href = "/login.html";
            // Успех
        } else {
            fname(xhr.responseText);
        }
    }
        
}

/** 
* Функция для обработки ошибок
* будет дописываться, пока вывод ошибок в консоль
*/
function setError22(txt, status) {
    var counter = 30; // через сколько секунд закрыть
    let par = $(".content-wrapper")[0];
    let addtxt = "";
    if (status != "" && status != undefined) {
        if (status == "400") {
            addtxt = "Ошибка в маршруте обращения к серверу, обратитесь к администратору!";
        }
        if (status == "405") {
            addtxt = "Метод HTTP протокола не реализован, попробуйте метод GET.";
        }

        if (status == "204") {
            addtxt = "Нет возвращаемых данных.";
        }
        if (status == "501") {

            addtxt = "Необходимо авторизоваться в системе";
            //   window.document.location.href = "/login.html";
        }


    }
    txt += "<br />" + addtxt;
    if (par != null) {
        let el = document.createElement("div");
        // el.classList.add("data_error");
        el.setAttribute('id', 'win1');
        el.classList.add("dm-overlay");
        el.style.display = 'block';
        el.style.zIndex = '9999999';

        let el2 = document.createElement("i"); // '<a class="close fa fa-close"></a>'
        el2.className = "fa fa-close close red_text";
        el2.onclick = function() {
            //el.style.display = 'none';
            if (el.parentNode) {
                el.parentNode.removeChild(el); // delete
            } else {
                el.remove();
            }
        }
        par.appendChild(el);

        error_block = '<div class="dm-table">' +
            '<div class="dm-cell">' +
            '<div class="dm-modal  madal_2x">' +
            '<div class="dm-modal-header "><h2 class="white_text" id="blink">Ошибка!</h2></div>' +
            '<div class="dm-modal-content red_text" style="font-size: 16pt;">' + txt + '</div>' +
            '<div class="dm-modal-footer center">' +
            '<button class="buttonM darkcyan_50" data-close>Продолжить<i data-timer="' + counter + '">(' + counter + ')</i></button> ' +
            '<button class="buttonM darkcyan_50" data-wait>Стоп!</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        el.insertAdjacentHTML("afterBegin", error_block);
        //var x_close = document.getElementById("blink");

        //var x_close = el.querySelector("h2");
        var x_close = el.querySelector(".dm-modal-header");

        x_close.appendChild(el2);
        //x_close.after(el2);
        // x_close.classList.toggle("hide");

        // Кнопка *Продолжить* делает тоже самое что и кнопка *х (закрыть)*
        el.querySelector('[data-close]').onclick = el2.onclick;
        // Запустить счетчик
        var timerId = setInterval(function() {
            // Уменьшить оставшееся время на 1
            var timer = el.querySelector('[data-timer]');
            var time = Number(timer.getAttribute('data-timer'));
            timer.setAttribute('data-timer', --time);
            timer.innerText = ' (' + time + ')';
            // По истечении времени
            if (time <= 0) {
                clearInterval(timerId); // удалить таймер
                el.querySelector('[data-close]').click(); // закрыть окно
            }
        }, 1000);
        // Кнопка *Стоп!*
        el.querySelector('[data-wait]').onclick = function(event) {
            event.target.style.display = 'none'; // скрыть кнопку *стоп*
            el.querySelector('[data-timer]').innerText = ''; // убрать из названия кнопки отсчитанные секунды
            clearInterval(timerId);
        };

    }
}

/**
 * Функция для обработки ошибок
 * @param {*} txt текст сообщения
 * @param {*} status  HTTP статус
 * @param {*} modalName идентификатор модального окошка, по умолчанию alertError
 */
function setError(txt, status, modalName) {
    return;
    // var counter = 30; // через сколько секунд закрыть
    modalName = modalName == undefined ? 'alertError' : modalName;
    let par = $(".content-wrapper")[0];
    let addtxt = "";
    if (status != "" && status != undefined) {
        if (status == 0) {
            addtxt = "Отсутствует связь с сервером!";
        } else if (status == "400") {
            addtxt = "Ошибка в маршруте обращения к серверу, обратитесь к администратору!";
        } else if (status == "405") {
            addtxt = "Метод HTTP протокола не реализован, попробуйте метод GET.";
        } else if (status == "204") {
            addtxt = "Нет возвращаемых данных.";
        } else if (status == "501") {
            //   window.document.location.href = "/login.html";
            addtxt = 'Соединение с сервером закрыто! <br />Все несохраненные данные будут утеряны. После нажатия на кнопку "Закрыть" вы будете переадресованны на страницу авторизации.';
        }

    }
    if (addtxt != undefined) {
        if (txt != "") {
            txt += "<br />" + addtxt;
        } else {
            txt = addtxt;
        }
    }
    let modal_arg = {
        headHTML: 'Ошибка!',
        bodyHTML: '<span style="font-size: 16pt; font-family:consolas">' + txt + '</span>',
        class: 'modal-lg',
        escapeble: "true",
        cnslName: "Закрыть",
        id: modalName
    };

    if (status == "501") {
        modal_arg.onclose = function(r) {
            window.document.location.href = '/login.html';
        }
    }
    modalOnce(modal_arg);

        let mdl=document.getElementById(modalName);
  //      mdl.classList.add("slowview");
        mdl.focus();
}

/**
 * Функция синхронного запроса к файлу на сервере
 * @deprecated
 * @param  string  url    - url
 * @param  JSON    r_args    - аргументы вида get: a=1&b=2&c=3... post: {"a":1, "b":2, "c":3, ...} (не реализовано)
 * @return JSON    - результат запроса
 * Пример использования:
 *     var struct = getJson('/api/cat/itemname');
 */
function getJson(url, r_args, method, sync, func) {
    sync = sync == undefined ? false : sync;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open(method || "GET", url, sync);
    xmlhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
    // xmlhttp.responseType = 'json';
    xmlhttp.send();

    // if ( xmlhttp.status == 501)
    // window.document.location.href = "/login.html";
    if (xmlhttp.status == 204) { 
        console.log('Запрос выполняется, пожалуйста, подождите ...');
        return {}; // повторные запросы
    }

    if (xmlhttp.status != 200 && xmlhttp.status != 501) {
        if (xmlhttp.status == 0 || xmlhttp.status == "0")
            error_txt = "Отсутствует связь с сервером! ";
        else
            error_txt = "Ошибка обращения к серверу, код ошибки " + xmlhttp.status;
        setError(error_txt, xmlhttp.status, 'getjsonError');
    } else if (xmlhttp.status == 501) {
        window.document.location.href = "/login.html";
    }
    //return JSON.parse(xmlhttp.response).data || [];
    let data = xmlhttp.response;
    // console.log(xmlhttp);
    try {
        data = JSON.parse(xmlhttp.response || '{}');
    } catch(e) { console.log(e); }
    if (!sync) return data || [];
}
function getHTML(url, r_args, method, sync, func) {
    sync = sync == undefined ? false : sync;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open(method || "GET", url, sync);
    xmlhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
    // xmlhttp.responseType = 'json';

    xmlhttp.send();

    // if ( xmlhttp.status == 501)
    // window.document.location.href = "/login.html";
    if (xmlhttp.status == 204) { 
        console.log('Запрос выполняется, пожалуйста, подождите ...');
        return {}; // повторные запросы
    }

    if (xmlhttp.status != 200 && xmlhttp.status != 501) {
        if (xmlhttp.status == 0 || xmlhttp.status == "0")
            error_txt = "Отсутствует связь с сервером! ";
        else
            error_txt = "Ошибка обращения к серверу, код ошибки " + xmlhttp.status;
        setError(error_txt, xmlhttp.status, 'getjsonError');
    } else if (xmlhttp.status == 501) {
        window.document.location.href = "/login.html";
    }
    //return JSON.parse(xmlhttp.response).data || [];
    // console.log(xmlhttp);
    let data = xmlhttp.response;
    try {
        // data = JSON.parse(xmlhttp.response || '{}');
    } catch(e) { console.log(e); }
    if (!sync) return data;
}

/** 
* Закрытие для любого объекта (по ID)
*/
function hideObj(obj) {
    let ob = $("#" + obj);
    if (ob === null)
        return;
    ob.style.display = 'none';
    let bl = $(".modal-back1");
    // let inner= $(".modal-inner2");
    while ($(".modal-back1")[0]) {
        document.body.removeChild($(".modal-back1")[0]);
    };
}


/**
 *  Функция обновления данных
 * @param {*} url УРЛ, к которому надо обратиться
 * @param {*} doc объект который должен отправиться на сервер
 * @param {*} funmane имя функции, обработчика, если не нужна то ''
 * @param {*} sync синхронный-асинхроный
 * @returns 
 */
function updData(url, doc, funmane, sync) {
    // let timerId=setTimeout(showLoader,900);
    sync = sync != undefined ? sync : true;
    var data = JSON.stringify(doc);
    var xhr = new XMLHttpRequest();
    var res = [];
    xhr.open("PUT", url, sync);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // clearTimeout(timerId);
            // hideLoader
            try {
                var json = JSON.parse(xhr.responseText);
                if (json.status.error_code == 0) {
                    if (funmane)
                        funmane(json);
                } else {
                    // clearTimeout(timerId);
                    // hideLoader
                    let tmx = "";
                    if (json.status.error_message_detail !="") {
                        tmx = json.status.error_message_detail.replace(/\n/g, "<br />") + "<br />";
                    }
                    let error_txt = "Ошибка: " + json.status.error_message.replace(/\n/g, "<br />") + "<br />" + tmx;
                    setError(error_txt, '', 'updDataError');
                }
            } catch (err) {
                // clearTimeout(timerId);
                // hideLoader
                let error_txt = "Ошибка разбора полученных данных!! " + err;
                setError(error_txt, xhr.status, 'updDataError2');
                console.log(err);
            }
        } else if (xhr.readyState != 1 && xhr.readyState != 0 && xhr.status != 200) {
            if (xhr.status == 204) { 
                console.log('Запрос выполняется, пожалуйста, подождите ...');
                showLoader();
                return {}; // повторные запросы
            }
            // clearTimeout(timerId);
            // hideLoader
            if(xhr.status == 413){
                hideLoader('_getDataError');
                setError("Размер вложения превышает допустимый предел!", xhr.status, '_getDataError');
                return;
            }
            let error_txt = "Ошибка получения данных! 1";
            setError(error_txt, xhr.status, 'updDataError1');
        }
    };
    try {
        xhr.send(data);
    } catch (err) {
        // clearTimeout(timerId);
        // hideLoader
        let error_txt = "Ошибка отправки запроса! " + err;
        setError(error_txt, 'updDataError');
    }
    try {
        if (sync == false) {
            res = JSON.parse(xhr.response) || [];
        }
    } catch (e) {
        // clearTimeout(timerId);
            // hideLoader
        console.log(e);
    }

    return res;
}


/**
 * Функция добавления данных
 * @param {*} ur УРЛ, к которому надо обратиться
 * @param {*} dt объект который должен отправиться на сервер
 * @param {*} funmane имя функции, обработчика, если не нужна то ''
 * @param {*} err_funct имя функции при ошибке
 */
function saveData(ur, dt, funmane,err_funct) {
   let timerId= setTimeout(showLoader,900);
    var data = JSON.stringify(dt);
    var xhr = new XMLHttpRequest();
    var url = ur;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.status == 204) { 
            console.log('Запрос выполняется, пожалуйста, подождите ...');
            return {}; // повторные запросы
        }
        if (xhr.readyState == 4 && xhr.status == 200) {
            clearTimeout(timerId);
            // hideLoader
            try {
                var json = JSON.parse(xhr.responseText);
                if (json.status.error_code == 0) {
                    if (funmane != '')
                        funmane(json);
                } else {
                    clearTimeout(timerId);
                    // hideLoader
                    let tmx = ""; //="";
                    console.log(json.status.error_code, json.status.error_message);
                    if (json.status.error_message_detail!="") {
                        tmx = json.status.error_message_detail.replace(/\n/g, "<br />") + "<br />";
                    }
                    let error_txt = "Ошибка: " + json.status.error_message + "<br />" + tmx;
                    setError(error_txt, '', 'saveDataError');
                }
            } catch (err) {
                clearTimeout(timerId);
                // hideLoader
                let error_txt = "Ошибка разбора полученных данных! " + err;
                setError(error_txt, xhr.status, 'saveDataError2');
                console.log(err);
            }
        } else if (xhr.status != 200 && xhr.status > 0) {
            clearTimeout(timerId);
            // hideLoader
            if(xhr.status == 413){
                hideLoader('_getDataError');
                setError("Размер вложения превышает допустимый предел!", xhr.status, '_getDataError');
                return;
            }
            let error_txt = "Ошибка получения данных! ";
            setError(error_txt, xhr.status, 'saveDataError1');
        }
    };
    try {
        xhr.send(data);
    } catch (err) {
        clearTimeout(timerId);
        // hideLoader
        let error_txt = "Ошибка отправки запроса! " + err;
        setError(error_txt, '', 'saveDataError');
    }
}


/**
 * Функция удаления данных
 * @param {*} ur  УРЛ, к которому надо обратиться
 * @param {*} dt  объект который должен отправиться на сервер
 * @param {*} funmane имя функции, обработчика, если не нужна то ''
 */
function delData(ur, dt, funmane) {
    let timerId=setTimeout(showLoader,900);
    var data = JSON.stringify(dt);
    var xhr = new XMLHttpRequest();
    var url = ur;;
    xhr.open("delete", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.status == 204) return; // повторные запросы
        if (xhr.readyState == 4 && xhr.status == 200) {
            clearTimeout(timerId);
            // hideLoader
            try {
                var json = JSON.parse(xhr.responseText);
                if (json.status.error_code == 0) {
                    if (funmane != '')
                        funmane(json);
                } else {
                    let error_txt = "Ошибка: " + json.status.error_message + "<br/>" + json.status.error_message_detail;
                    setError(error_txt, '', 'delDataError');
                }
            } catch (err) {
                let error_txt = "Ошибка разбора полученных данных! " + err;
                setError(error_txt, xhr.status, 'delDataError2');
            }
        } else if (xhr.readyState != 1 && xhr.readyState != 0 && xhr.status != 200) {
            clearTimeout(timerId);
            // hideLoader
            if(xhr.status == 413){
                hideLoader('_getDataError');
                setError("Размер вложения превышает допустимый предел!", xhr.status, '_getDataError');
                return;
            }
            let error_txt = "Ошибка получения данных! ";
            setError(error_txt, xhr.status, 'delDataError1');
        }
    };
    try {
        xhr.send(data);
    } catch (err) {
        clearTimeout(timerId);
        // hideLoader
        let error_txt = "Ошибка отправки запроса! " + err;
        setError(error_txt, '', 'delDataError');
    }
}

/**
 * полностью настраиваимая версия запроса
 * @param {*} url   УРЛ, к которому надо обратиться
 * @param {*} method метод (GET, POST и т.д.)
 * @param {*} data данные
 * @param {*} funmane  имя функции, обработчика, если не нужна то ''
 * @param {*} fnameError  имя функции при ошибке
 */
function queryData(url, method, data, funmane, fnameError) {
    let timerId= setTimeout(showLoader,900);
    data = JSON.stringify(dt);
    let xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.status == 204) return; // повторные запросы
        clearTimeout(timerId);
        // hideLoader
        if (xhr.readyState == 4 && xhr.status == 200) {
            try {
                let json = JSON.parse(xhr.responseText);
                if (json.status.error_code == 0) {
                    if (funmane) funmane(json);
                } else {
                    setError("Ошибка: " + json.status.error_message + "<br>" + (json.status.error_message_detail.replace(/\n/g, "<br>") + "<br>" ), '', 'saveDataError');
                    if (fnameError) fnameError(json);
                }
            } catch (err) {
                setError("Ошибка разбора полученных данных! " + err, xhr.status, 'saveDataError2');
            }
        } else if (xhr.status != 200 && xhr.status > 0) {
            setError("Ошибка получения данных! ", xhr.status, 'saveDataError1');
        }
    };
    try {
        xhr.send(data);
    } catch (err) {
        clearTimeout(timerId);
        // hideLoader
        setError("Ошибка отправки запроса! " + err, '', 'saveDataError');
    }
}

/**
 * Функция возвращаюшая значение параметра по именени  если параметр не найден - возварщается false
 * @param {*} key название параметра
 * @returns 
 */
function $_GET(key) {
    var p = window.location.search;
    p = p.match(new RegExp(key + '=([^&=]+)'));
    return p ? p[1] : false;
}

/**
 *Функция проверки заполнености какими то данными
 *полей по их классу. Класс у поля должен быть "required"
 * @param  string  clss  - css класс элемента, в котром расположенны проверяемые поля
 * @return string  parent    - id элемента верхнего уровня, в котром расположенны проверяемые поля
 * если все найденные поля заполнены - то true, если хоть одно не заполнено - то false
 */
function chekRequired(clss, parent) {
    let parnt = $("#" + parent);
    let isFind = false;
    let elm;
    let rf = parnt.querySelectorAll(".required");

    for (var key in rf) {
        let el = rf[key];
        if (el != null && el.tagName == "INPUT") {
            elm = el;
            isFind = true;
        } else {
            if (el != null && el.childNodes != undefined && el.childNodes.length > 0) {
                el.childNodes.forEach(function(ch) {
                    if (ch.tagName == "INPUT") {
                        elm = ch;
                        isFind = true;
                    }
                });
            }
        }

        if (isFind && el.classList != undefined) {
            if (elm.value.length > 0) {
                el.classList.remove("requiredEror");
            } else {
                el.classList.add("requiredEror");
                return false;
            }
        }
    }
    return true;
}

/**
 *Функция проверки заполнености любых полей какими то данными
 *полей по их классу. Класс у поля должен быть "required"
 * @param  string  clss  - css класс элемента, в котром расположенны проверяемые поля
 * @return string  parent    - id элемента верхнего уровня, в котром расположенны проверяемые поля
 * если все найденные поля заполнены - то true, если хоть одно не заполнено - то false
 */
function chekRequiredAll(clss, parent) {
    let parnt = $("#" + parent);
    //console.log('parnt=', parnt);
    let isFind = false;
    let elm;
    let rf = parnt.querySelectorAll(".required");

    for (var key in rf) {
        let el = rf[key];
        if (el != null && (el.tagName == "INPUT" || el.tagName == "SELECT")) {
            elm = el;
            isFind = true;
        } else {
            if (el != null && el.childNodes != undefined && el.childNodes.length > 0) {
                el.childNodes.forEach(function(ch) {
                    if (ch.tagName == "INPUT" || ch.tagName == "SELECT") {
                        elm = ch;
                        isFind = true;
                    }
                });
            }
        }

        if (isFind && el.classList != undefined) {
            if (elm.value.length > 0) {
                el.classList.remove("requiredEror");
            } else {
                el.classList.add("requiredEror");
                return false;
            }
        }
    }
    return true;
}

/**
 * Функция автоматического продления блокировки
 * @param string idc идентификатор документа, который надо заблокировать
 * LOC_TIMER - глобавльный таймер
 * T_DELAY - глобальное время опроса таймера
 */
function autoResume(idc, url) {
    if (url === undefined || url === null) {
        url = "/api/doc/proposal/lock";
    }
    LOC_TIMER = setInterval(function() {
        var doc = { arguments: { doc_id: idc } }; //, dac_code: 'NOOP'
        updData(url, doc, '');
    }, T_DELAY);
}

/**
 * Функция остановки глобального таймера
 *  LOC_TIMER - глобавльный таймер
 */
function stopLocTimer() {
    clearTimeout(LOC_TIMER);
}

/**
 * Функция подписи авторизованного пользователя
 *
 */
function setUsrName() {
    let dat = localStorage.getItem('cruusr');
    if (dat == null)
        window.document.location.href = "/login.html";
    let usr = JSON.parse(dat);
    document.querySelector(".user_li").innerText = usr.sbj_short_name;
    document.querySelector("#footer_S_org_name").innerText = usr.sorgName;
    document.querySelector("#footer_S_division_name").innerText = usr.div_short_name;

}


/**
 * Функция getCookie  возвращает куки с указанным name
 * @author Vododokhov
 * @param  name название куки
 * Пример использования:
 *     getCookie('language');
 */
function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


/**
 * Функция setCookie Устанавливает куки с именем name и значением value, с настройкой path=/ по умолчанию (можно изменить, чтобы добавить другие значения по умолчанию)
 * @author Vododokhov
 * @param  name название куки
 * @param  value
 * @param  options
 * Пример использования:
 *     setCookie('user', 'John', {secure: true, 'max-age': 3600});
 */
function setCookie(name, value, options) {
    options = options || {};
    options = Object.assign({}, {
        path: '/',
        // при необходимости добавьте другие значения по умолчанию
        // ...options
    }, options);

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}


/**
 * Функция deleteCookie удаление куков
 * @author Vododokhov
 * @param  name название куки
 * Пример использования:
 *     deleteCookie('language');
 */
function deleteCookie(name) {
    setCookie(name, "", {
        'max-age': -1
    })
}

/**
 * Функция преобразования даты в формат dd.MM.yyyy
 * @author Dmitriy Sharshak
 * @return string - dd.MM.yyyy
 */
function getDate8Digits(date, isShowTime) {
    var d = new Date(date);

    var hours = d.getHours();
    hours = hours < 10 ? '0' + hours : hours;

    var minutes = d.getMinutes();
    minutes = minutes < 10 ? '0' + minutes : minutes;

    var day = d.getDate();
    day = day < 10 ? '0' + day : day;

    var month = d.getMonth() + 1;
    month = month < 10 ? '0' + month : month;

    var year = d.getFullYear();

    var dd = [day, month, year];
    var time = [hours, minutes];

    if (!isShowTime)
        return dd.join('.');
    else
        return dd.join('.') + " " + time.join(':');
}

/**
 * Функция генерации случайного уникального UUID
 * @author Nelly
 * @return string - UUID
 * Пример использования:
 *     console.log(createUUID()); // 11d4713f-c7f3-419d-bf91-36982fc83e36
 */
function createUUID() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function(c) {
        return (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    });
}

let lockDocumentTimer = {};

/**
 * Функция создания документа
 * @author Nelly
 * @param  settings - аргументы
 * @return status
 * Пример использования (простой):
 *     let result = documentAdd({ api:'/api/doc/proposal/add', code:'OCAT', params:{...} });
 * Пример использования:
 *     documentAdd({
 *         api: '/api/doc/proposal/add', // api !!!ОБЯЗАТЕЛЬНЫЙ
 *         code: 'OCAT',                 // тип документа (dot_code) !!!ОБЯЗАТЕЛЬНЫЙ
 *         params: {                     // неатандартные параметры документа !!!ОБЯЗАТЕЛЬНЫЙ
 *             ctlo_dcn: "",
 *             ctlo_ekps: "",
 *             ctlo_inc: "05970",
 *             ctlo_note: "",
 *             ctlo_okp: "",
 *             ctlo_orig_name: "Клещи #3181",
 *             ctlo_source: "",
 *             doc_date: "2020-05-07",
 *         },
 *         doc_links: [            // массив связей документа
 *             {
 *                 doc_id_1: "0ca077b4-e5bb-426b-94b6-95985bed7072",
 *                 doc_id_2: "35d3b7d3-a665-499f-b2bc-2f1862758f22",
 *                 doc_name_1: "SPLR № SPLR5120570000001_1 от 31.01.2020 г.",
 *                 doc_name_2: "CDSC № items5120570000001 от 30.01.2020 г.",
 *                 dot_code_1: "SPLR",
 *                 dot_code_2: "CDSC",
 *                 lnt_code: "SPPL",
 *             },
 *         ],
 *         document_sides: [            // массив сторон документа
 *             {
 *                 dtrl_code: "CATL",
 *                 psdr_mandatory: 1,
 *                 psdr_type: "",
 *                 sbj_id: "0051fa90-fde1-47c4-ae55-390d3a4733c9",
 *                 sbj_name: "Акционерное общество \"Авиаагрегат\"",
 *                 sbj_short_name: "АО \"Авиаагрегат\"",
 *             },
 *         ], 
 *         struct: '/api/doc/proposal/struct', // структура, для проверок
 *         success: function(answer){                        // функция, выполняющаяся при успехе
 *             messageReq('Заявка успешно создана.');
 *         },
 *         error: function(answer){                          // функция, выполняющаяся при возникновении ошибок (по умолчанию выдается модал о ошибкой)
 *             setError(answer.status.error_message, answer.status.error_code);
 *         },
 *     }
 * Пример выходных данных:
 *    {
 *        data: { ctlo_inc: "05970", ctlo_orig_name: "Кувалда #8941", doc_date: "2020-05-07", … }
 *        debug: { elapsed_time: 126 }
 *        status: { error_code: 0, error_message: "", total_rows: 1 }
 *    }
 */
function documentAdd(settings) {
    if (!settings.code || !settings.params || !settings.api) return false;
    // settings.doc_links = settings.doc_links ? settings.doc_links : [];
    // settings.document_sides = settings.document_sides ? settings.document_sides : [];
    // Загрузить структуру, если она задана в виде апи
    if (settings.struct && typeof(settings.struct) == 'string') {
        getData(settings.struct, { error_txt: "", data: "", status: "", result: true, isOk: function() { return this.result; } }, function(answer) {
            settings.struct = answer.data;
            documentAdd(settings);
        });
        return 0;
    }

    // Стандартные поля (?) // если в settings.params будут такие же, будет заменено на то что в settings.params
    let standard = {
        doc_date: (new Date()).toISOString().substring(0, 10), // 2020-05-07 (текущая дата)
        // doc_id: null,
        doc_number: "б/н",
        doc_links: settings.doc_links || [],
        document_sides: settings.document_sides || [],
        dot_code: settings.code,
        stt_code_1: "NDFN",
        stt_code_2: "DRFT",
        stt_name_1: null,
        stt_name_2: null,
    }

    // Объединить входные данные
    let arguments1 = Object.assign({}, standard, settings.params);
    arguments1.doc_links = standard.doc_links;
    arguments1.document_sides = standard.document_sides; //standard.document_sides.concat(settings.document_sides);

    // Проверки по структуре
    // if (settings.struct) {
    //     arguments1 = documentFilter(arguments, settings.struct);
    // }
    //console.info('%c<document>%c Создание:\n',`color:#7B68EE`, `color:#4682B4`, settings.api, { arguments: arguments });
    // Отправка на сервер
    saveData(settings.api, { arguments: arguments1 }, function(answer) {
        // функции обработки ответа (если указаны)
        if (answer.status.error_code == 0 && settings.success && typeof(settings.success) == 'function') {
            settings.success(answer);
        }
        if (answer.status.error_code != 0) {
            if (settings.error && typeof(settings.error) == 'function') {
                settings.error(answer);
            } else {
                setError(answer.status.error_message + "<br/>" + answer.status.error_message_detail, answer.status.error_code, 'documentAddError');
            }
        }
    });
    // return answer;
}

/**
 * Функция изменения документа
 * @author Nelly
 * @param  settings - аргументы
 * @return status
 * Пример использования:
 *     let request = {doc_id:'...', ...};
 *     documentUpdate({
 *         api: '/api/doc/proposal/add', // api !!!ОБЯЗАТЕЛЬНЫЙ
 *         document: request,           // старые значения (ПОЛНОСТЬЮ) !!!ОБЯЗАТЕЛЬНЫЙ
 *         params: {                     // новые значения (можно частично) !!!ОБЯЗАТЕЛЬНЫЙ
 *             ctlo_dcn: "",
 *             ctlo_ekps: "",
 *             ctlo_inc: "05970",
 *             ctlo_note: "",
 *             ctlo_okp: "",
 *             ctlo_orig_name: "Клещи #3181",
 *             ctlo_source: "",
 *             doc_date: "2020-05-07",
 *         },
 *         document_sides: [             // массив сторон документа (новый)
 *             {
 *                 dtrl_code: "CATL",
 *                 psdr_mandatory: 1,
 *                 psdr_type: "",
 *                 sbj_id: "0051fa90-fde1-47c4-ae55-390d3a4733c9",
 *                 sbj_name: "Акционерное общество \"Авиаагрегат\"",
 *                 sbj_short_name: "АО \"Авиаагрегат\"",
 *             },
 *         ],
 *         struct: getJson('/api/doc/proposal/struct').data, // структура, для проверок
 *         success: function(answer){                        // функция, выполняющаяся при успехе
 *             messageReq('Заявка успешно изменена.');
 *             documentUnlock({document:request}); // блокировка
 *         },
 *         error: function(answer){                          // функция, выполняющаяся при возникновении ошибок (по умолчанию выдается модал о ошибкой)
 *             setError(answer.status.error_message, answer.status.error_code);
 *         },
 *     }
 * Пример выходных данных:
 *    {
 *        data: { ctlo_inc: "05970", ctlo_orig_name: "Кувалда #8941", doc_date: "2020-05-07", … }
 *        debug: { elapsed_time: 126 }
 *        status: { error_code: 0, error_message: "", total_rows: 1 }
 *    }
 */
function documentUpdate(settings) {//console.log(settings);
    if (!settings.api) return false;
    if (!settings['params']) {
        settings['params'] = {};
    }
    // Загрузить структуру, если она задана в виде апи
    if (settings.struct && typeof(settings.struct) == 'string') {
        getData(settings.struct, null, function(answer) {
            settings.struct = answer.data;
            documentUpdate(settings);
        });
        return 0;
    }
    // Загрузить документ, если он задан в виде апи
    if (settings.document && typeof(settings.document) == 'string') {
        getData(settings.document, null, function(answer) {
            settings.document = answer.data;
            documentUpdate(settings);
        });
        return 0;
    }
    // if (settings.document.stt_code_2 != 'DRFT') {
    // setError('Изменение документов допустимо только в состоянии "Черновик"');
    // return false;
    // }
    settings['params']['document_sides'] = [];
    settings['params']['doc_links'] = [];
    settings['params'].document_sides = settings.document_sides || settings.document.document_sides || [];
    settings['params'].doc_links = settings.doc_links || settings.document.doc_links || [];

    // Стандартные поля (?) // если в settings.params будут такие же, будет заменено на то что в settings.params
    let standard = settings.document;
    // Объединить входные данные
    let arguments2 = Object.assign({}, standard, settings['params']);
    // Проверки по структуре
    // if (settings.struct) {
    //     arguments2 = documentFilter(arguments, settings.struct);
    // }
    // указать совершаемое действие
    Object.assign(arguments2, { dac_code: 'ENDE' });
    // settings.document.doc_id = settings.document.doc_id || null;
    if (settings.document.doc_id) {
        //console.info(`%c<document> %cИзменение: %c${settings.document.doc_id}\n`,`color:#7B68EE`, `color:#4682B4`, `color:inherit`, settings.api, { arguments: arguments });
    }
    // Отправка на сервер
    updData(settings.api, { arguments: arguments2 }, function(answer) {
        // функции обработки ответа (если указаны)
        if (answer.status.error_code == 0 && settings.success && typeof(settings.success) == 'function') {
            try { settings.success(answer); } catch (e) { console.error(e); }
        }
        if (answer.status.error_code != 0) {
            if (settings.error && typeof(settings.error) == 'function') {
                try { settings.error(answer); } catch (e) { console.error(e); }
            } else {
                setError(answer.status.error_message+ "<br/>" + answer.status.error_message_detail, answer.status.error_code, 'documentUpdateError');
            }
        }
    });

    // return answer;
}

/**
 * Функция удаления документа
 * @author Nelly
 * @param  settings - аргументы
 * @return status
 * Пример использования:
 *     documentDelete({
 *         api: '/api/doc/proposal/add', // api !!!ОБЯЗАТЕЛЬНЫЙ
 *         document: {doc_id:'...', ...},              // старые значения (ПОЛНОСТЬЮ) !!!ОБЯЗАТЕЛЬНЫЙ
 *         success: function(answer){                        // функция, выполняющаяся при успехе
 *             messageReq('Заявка успешно изменена.');
 *         },
 *         error: function(answer){                          // функция, выполняющаяся при возникновении ошибок (по умолчанию выдается модал о ошибкой)
 *             setError(answer.status.error_message, answer.status.error_code);
 *         },
 *     }
 * Пример выходных данных:
 *    {
 *        data: { ctlo_inc: "05970", ctlo_orig_name: "Кувалда #8941", doc_date: "2020-05-07", … }
 *        debug: { elapsed_time: 126 }
 *        status: { error_code: 0, error_message: "", total_rows: 1 }
 *    }
 */
function documentDelete(settings) {
    if (!settings.api) return false;
    // Загрузить документ, если он задан в виде апи
    if (settings.document && typeof(settings.document) == 'string') {
        getData(settings.document, null, function(answer) {
            settings.document = answer.data;
            documentDelete(settings);
        });
        return 0;
    }
    // if (settings.document.stt_code_2 != 'DRFT') {
    //     setError('Удаление документов допустимо только в состоянии "Черновик"', '', 'documentDelError');
    //     return false;
    // }

    // Объединить входные данные
    let arguments3 = settings.document;
    //console.info(`%c<document> %cУдаление: %c${settings.document.doc_id}\n`,`color:#7B68EE`, `color:#4682B4`, `color:inherit`, settings.api, { arguments: arguments });
    // Отправка на сервер
    delData(settings.api, { arguments: arguments3 }, function(answer) {
        // функции обработки ответа (если указаны)
        if (answer.status.error_code == 0 && settings.success && typeof(settings.success) == 'function') {
            settings.success(answer);
        }
        if (answer.status.error_code != 0) {
            if (settings.error && typeof(settings.error) == 'function') {
                settings.error(answer);
            } else {
                setError(answer.status.error_message+ "<br/>" + answer.status.error_message_detail, answer.status.error_code, 'documentDelError');
            }
        }
    });

    // return answer;
}

/**
 * Функция изменения статуса документа
 * @author Nelly
 * @param  settings - аргументы
 * @return status
 * Пример использования:
 *     documentSetStatus({
 *         api: '/api/doc/proposal/add', // api // если особенное
 *         document: {doc_id:'...', ...},              // старые значения (ПОЛНОСТЬЮ) !!!ОБЯЗАТЕЛЬНЫЙ
 *         status: 'DRFT',               // код статуса !!!ОБЯЗАТЕЛЬНЫЙ
 *         success: function(answer){                        // функция, выполняющаяся при успехе
 *             messageReq('Заявка успешно изменена.');
 *         },
 *         error: function(answer){                          // функция, выполняющаяся при возникновении ошибок (по умолчанию выдается модал о ошибкой)
 *             setError(answer.status.error_message, answer.status.error_code);
 *         },
 *     }
 * Пример выходных данных:
 *    {
 *        data: { ctlo_inc: "05970", ctlo_orig_name: "Кувалда #8941", doc_date: "2020-05-07", … }
 *        debug: { elapsed_time: 126 }
 *        status: { error_code: 0, error_message: "", total_rows: 1 }
 *    }
 */
function documentSetStatus(settings) {
    if (!settings.status) return false;
    // Загрузить документ, если он задан в виде апи
    if (settings.document && typeof(settings.document) == 'string') {
        getData(settings.document, null, function(answer) {
            settings.document = answer.data;
            documentSetStatus(settings);
        });
        return 0;
    }
    settings.api = settings.api || '/api/doc/document/action';
    // if(settings.status == 'EDIT' && getFromStorage('lock_doc_'+settings.document.doc_id, {is_edit_now:false}).is_edit_now == true){
    //     setError('Документ уже редактируется в текущей сессии.');
    //     if (settings.error && typeof (settings.error) == 'function') {
    //         settings.error({});
    //     }
    //     return false;
    // }
    // else if(settings.status == 'EDIT') {
    //     saveToLocalStorage('lock_doc_'+settings.document.doc_id, {is_edit_now:true});
    // }

    // Объединить входные данные
    let arguments4 = Object.assign({}, settings.document, { dac_code: settings.status }, { cma_note:settings.comment });
    // console.log(arguments, settings);
    //console.info(`%c<document> %cИзменение статуса (${settings.status}): %c${settings.document.doc_id}\n`,`color:#7B68EE`, `color:#4682B4`, `color:inherit`, settings.api, { arguments: arguments });
    // Отправка на сервер
    updData(settings.api, { arguments: arguments4 }, function(answer) {
        // функции обработки ответа (если указаны)
        if (answer.status.error_code == 0 && settings.success && typeof(settings.success) == 'function') {
            settings.success(answer);
        }
        if (answer.status.error_code != 0) {
            if (settings.error && typeof(settings.error) == 'function') {
                settings.error(answer);
            } else {
                setError(answer.status.error_message+ "<br/>" + answer.status.error_message_detail, answer.status.error_code, 'documentSetStatusError');
            }
        }
    });

    // return answer;
}

/**
 * Функция блокировки документа (отдельная, используется внутри documentBlock)
 * @author Nelly
 * @param  settings - аргументы
 * @return status
 * Пример использования (предпочтительный):
 *     let request = {doc_id:'...', ...};
 *     documentLock({
 *         api: '/api/doc/proposal/lock',
 *         api_2: '/api/doc/proposal/action',
 *         document: request,
 *         success: function(answer){
 *             console.log("--- Заблокировать ---\n", request.doc_id);
 *         },
 *     });
 */
function documentLock(settings) {
    settings.api = settings.api || '/api/doc/document/lock';
    if (!settings.api) return false;
    // Загрузить документ, если он задан в виде апи
    if (settings.document && typeof(settings.document) == 'string') {
        getData(settings.document, null, function(answer) {
            settings.document = answer.data;
            documentLock(settings);
        });
        return 0;
    }
    // Объединить входные данные
    let arguments5 = Object.assign({}, settings.document, { dac_code: "NOOP" });

    // if (getFromStorage('lock_doc_' + settings.document.doc_id) == true) {
    //     setError('Документ уже редактируется в текущей сессии.');
    //     if (settings.error && typeof(settings.error) == 'function') {
    //         settings.error({});
    //     }
    //     return false;
    // } else {
    //     saveToLocalStorage('lock_doc_' + settings.document.doc_id, true);
    // }

    // Первая попытка заблокировать документ
    updData(settings.api, { arguments: arguments5 }, function(answer1) {
        let answer = answer1;
        // функции обработки ответа (если указаны)
        if (answer.status.error_code == 0 && settings.success && typeof(settings.success) == 'function') {
            //console.info(`%c<document> %cБлокировка: %c${settings.document.doc_id}\n`,`color:#7B68EE`, `color:#4682B4`, `color:inherit`, settings.api);
            settings.success(answer);
        }
        if (answer.status.error_code != 0) {
            if (settings.error && typeof(settings.error) == 'function') {
                settings.error(answer);
            } else {
                setError(answer.status.error_message+ "<br/>" + answer.status.error_message_detail, answer.status.error_code, 'documentLockError');
            }
        }

        if (!lockDocumentTimer[settings.document.doc_id]) lockDocumentTimer[settings.document.doc_id] = {};
        // Запуск автопродления блокировки (только если первая попытка успешна)
        if (answer.status.error_code == 0) {
            lockDocumentTimer[settings.document.doc_id].timer = setInterval(function() {
                updData(settings.api, { arguments: settings.document }, function(answer2) {
                    let lock = answer2;
                    if (lock.error && typeof(lock.error) == 'function') {
                        lock.error(answer);
                    } else {
                        //console.info(`%c<document> %cПродление блокировки: %c${settings.document.doc_id}\n`,`color:#7B68EE`, `color:#4682B4`, `color:inherit`, settings.api);
                        //setError(lock.status.error_message, lock.status.error_code);
                    }
                });
            }, T_DELAY /*T_DELAY*/ );
        }
        // Снятие блокировки при уходе со страницы
        lockDocumentTimer[settings.document.doc_id].cancel = function() {
            saveToLocalStorage('lock_doc_' + settings.document.doc_id, false);
            if (settings.api_2) {
                documentSetStatus({
                    api: settings.api_2,
                    document: settings.document,
                    status: 'CNLE',
                });
                documentUnlock({
                    document: settings.document, //settings.document,
                    success: function() {
                        //console.info(`%c<document> %cСнятие блокировки: %c${settings.document.doc_id}`,`color:#7B68EE`, `color:#4682B4`, `color:inherit`, settings.api_2);
                    },
                });
            }
        }
        window.onbeforeunload = function() {
            return false;
        };
        window.addEventListener("unload", lockDocumentTimer[settings.document.doc_id].cancel);
    });

    // return answer;
}

/**
 * Функция перевода документа в статус  *Редактируется* + блокировка
 * @author Nelly
 * @param  settings - аргументы
 * Пример использования: (вместе с сохранением после редактирования и разблокировкой при закрытии/отмене)
 *     documentBlock({
 *          api_action: '/api/doc/proposal/action',
 *          api_lock: '/api/doc/proposal/lock',
 *          document: request,
 *          success: function(answer) {
 *              request = answer ? answer.data : request;
 *              modalOnce({
 *                  headHTML: 'Редактирование заявки',
 *                  body: (REQUEST.templates.querySelector('#req')).cloneNode(true),
 *                  class: 'madal_2x',
 *                  id: 'edt_modal',
 *                  footer: [{
 *                      description: `Сохранить`,
 *                      onclick: function() {
 *                          let form = document.querySelector('#req');
 *                          // Обновление документа
 *                          documentUpdate({
 *                              api: '/api/doc/proposal/action',
 *                              document: request,
 *                              params: form.getValues(),
 *                              document_sides: [...],
 *                              struct: REQUEST.structD,
 *                              success: function(data) {
 *                                  REQUEST.table.rowUpdate(id, data.data); // Обновить строку в таблице
 *                                  documentUnlock({ document: data.data });
 *                                  modalDelete('edt_modal');
 *                              },
 *                          });
 *                      }
 *                  }],
 *                  onclose: function() {
 *                      // Отмена редактирования и перевод в состояние черновика
 *                      documentSetStatus({
 *                          api: '/api/doc/proposal/action',
 *                          document: request,
 *                          status: 'CNLE',
 *                          success: function(req2) {
 *                              REQUEST.table.rowUpdate(req2.data.doc_id, req2.data); // Обновить строку в таблице
 *                          }
 *                      });
 *                      // Снятие блокировки
 *                      documentUnlock({ document: request });
 *                      modalDelete('edt_modal');
 *                  }
 *              });
 *          },
 *          error: function(e) { console.log(e); } // Функция при ошибке блокировки
 *      });
 */
function documentBlock(settings) {
    if (!settings.api_action || !settings.document) return false;
    // Попытка перевести документ в режим редактирования
    documentSetStatus({
        api: settings.api_action,
        document: settings.document,
        comment: settings.comment,
        status: 'EDIT',
        success: function(answer) {
            documentLock({
                api: settings.api_lock,
                api_2: settings.api_action, // api для разблокировки при обновлении страницы
                document: answer.data || settings.document,
                success: function(answer) {
                    if (settings.success) {
                        settings.success(answer);
                    }
                },
                error: settings.error
            });
        },
        error: settings.error
    });
}

/**
 * Функция снятия блокировки с документа
 * @author Nelly
 * @param  settings - аргументы
 * Пример использования:
 *   let request = {doc_id:'...', ...};
 *   documentUnlock({
 *       document: request,
 *       success: function(){
 *           console.log("--- Разблокировать ---\n", request.doc_id);
 *       },
 *   });
 */
function documentUnlock(settings) {
    // Загрузить документ, если он задан в виде апи
    if (settings.document && typeof(settings.document) == 'string') {
        getData(settings.document, null, function(answer) {
            settings.document = answer.data;
            documentUnlock(settings);
        });
        return 0;
    }
    if (!lockDocumentTimer[settings.document.doc_id]) lockDocumentTimer[settings.document.doc_id] = {};
    saveToLocalStorage('lock_doc_' + settings.document.doc_id, false);

    clearTimeout(lockDocumentTimer[settings.document.doc_id].timer);

    // функции обработки ответа (если указаны)
    if (settings.success && typeof(settings.success) == 'function') {
        settings.success();
    } else {
        //console.info(`%c<document> %cСнятие блокировки: %c${settings.document.doc_id}\n`,`color:#7B68EE`, `color:#4682B4`, `color:inherit`,);
    }

    // Снятие блокировки при уходе со страницы
    window.onbeforeunload = false;
    window.removeEventListener("unload", lockDocumentTimer[settings.document.doc_id].cancel);
}

/**
 * Функция Снятия блокировки с нестандартного документа
 * @author Nelly
 * @param  settings - аргументы
 * Пример использования:
    docUnlock("088ab6ad-e6a0-4a62-83fb-0593af11a25a", function(answer1){
        console.log(answer1);
    });
 */
function docUnlock(docId, func){
    updData('/api/doc/document/unlock', { arguments: { doc_id: docId } }, func);
}

/**
 * Функция проверки и приведения типов данных документа
 * @author Nelly
 * @param  arguments - аргументы
 * @param  struct    - структура
 * @return status
 */
function documentFilter(arguments, struct) {
    if (!struct) {
        return arguments;
    }

    // Отфильтровать массив значений: взять только те, что присутствуют в структуре
    Object.keys(arguments).forEach(function(i) {
        let is_in_struct = struct.find(function(it, key) {
            if (it.name == key) return true;
            else if (typeof(it) == 'object' && Object.keys(it).length == 1 && Object.keys(it)[0] == key) {
                return true;
            }
        });
        if (!is_in_struct) {
            delete arguments[i];
        }
    });
    // arguments = arguments.filter(function (item, key) {
    //     let is_in_struct = struct.find(function (it) {
    //         if (it.name == key) return true;
    //         else if(typeof(it) == 'object' && Object.keys(it).length == 1 && Object.keys(it)[0] == key){
    //             return true;
    //         }
    //     });
    //     if (is_in_struct) {
    //         return true;
    //     }
    // });
    // Приведение типов
    let parse = function(struct_2, data) {
        (Array.from(struct_2) || []).forEach(function(it) {
            if (!data[it.name]) {
                // data[it.name] = null;
                return true;
            }
            switch (it.data_type) {
                case 'integer':
                    data[it.name] = Number(data[it.name]);
                    break;
                case 'boolean':
                    data[it.name] = Boolean(data[it.name]);
                    break;
                case 'array':
                    data[it.name] = (Array.from(data[it.name]) || []).map(function(i) {
                        if (it[it.name]) {
                            let t = parse(it[it.name], i);
                            return t;
                        }
                        // else { return i; }
                    });
                    data[it.name] = (Array.from(data[it.name]) || []).filter(function(i) { return i; });
                    break;
                case 'string':
                    // data[it.name] = htmlspecialchars(data[it.name]);
                    data[it.name] = data[it.name];
                    break;
                case 'date':
                default:
                    if (typeof(it) == 'object' && Object.keys(it).length == 1) {
                        let name = Object.keys(it)[0];
                        data[name] = parse(it[name], data[name]);
                    }
                    break;
            }
        });
        return data;
    }
    arguments = parse(struct, arguments);
    return arguments;
}
/**
 * получение параметров по ключу с очисткой
 * @param {*} key ключь
 * @returns 
 */
function GETparam(key) {
    var p = window.location.search;
    p = p.match(new RegExp(key + '=([^&=]+)'));
    return p ? p[1] : false;
}

/**
 * Скрывает элемент.
 * Текущее значение style.display сохраняется в свойстве oldDisplayStyle.
 * @author Anatoliy Yakimov
 */
Object.defineProperty(HTMLElement.prototype, 'hide', {
    value: function() {
        if (this.style.display !== 'none') {
            this.oldDisplayStyle = this.style.display;
            this.style.display = 'none';
        }
        return this;
    }
});

/**
 * Показывает элемент.
 * Если объект был скрыт методом hide(), то предыдущее значение style.display будет восстановлено
 * @author Anatoliy Yakimov
 */
Object.defineProperty(HTMLElement.prototype, 'show', {
    value: function() {
        //Свойство oldDisplayStyle выставляется в методе hide()
        if (this.style.display === 'none') {
            this.style.display = this.oldDisplayStyle ? this.oldDisplayStyle : '';
        }
        return this;
    }
});

/**
 * Если элемент скрыт, то показывает его. Иначе, скрывает.
 * @author Anatoliy Yakimov
 */
Object.defineProperty(HTMLElement.prototype, 'toggle', {
    value: function() {
        return this.style.display === 'none' ? this.show() : this.hide();
    }
});

/**
 * Динамически загружает css файл.
 * @author Anatoliy Yakimov
 * @param path путь к css файлу
 */
function loadCss(path) {
    let link = document.createElement('link');
    link.type = 'text/css';
    link.href = path;
    link.media = 'all';
    link.rel = 'stylesheet';
    document.getElementsByTagName('head')[0].appendChild(link);
}

/**
 * Извлекает элемент из локального хранилища.
 * Если в хранилище нет такого элемента, то возвращается defaultValue
 * @author Anatoliy Yakimov
 * @param key ключ объекта в хранилище
 * @param defaultValue объект, который вернётся, если объект в хранилище не обнаружен
 * @returns объект из хранилища или defaultValue
 */
function getFromStorage(key, defaultValue) {
    let val = localStorage.getItem(key);
    return val && val != 'undefined' ? JSON.parse(val) : defaultValue;
}

/**
 * Помещает объект в хранилище
 * @author Anatoliy Yakimov
 * @param key ключ объекта в хранилище
 * @param value объект
 */
function saveToLocalStorage(key, value) {
    localStorage.setItem(key, value ? JSON.stringify(value) : undefined);
}

/**
 * Удаляет объект из локального хранилища.
 * @author Anatoliy Yakimov
 * @param key ключ объекта
 */
function deleteFromStorage(key) {
    localStorage.removeItem(key);
}
// try {
//     if(!localStorage)
//     var localStorage = {
//         getItem: 
//     }
// }

/**
 * Аналог $.expand(obj, args) из jQuery.
 * Нерекурсивно объединяет содержимое двух или более объектов в один объект.
 * Удобно использовать для дополнения настроек пользователя дефолтными настройками.
 * Пример:
 * var defaultOptions = {
 *     opt1: true,
 *     opt2: 'banana'
 * };
 * var options = {
 *     opt1: false
 * }
 * options = Object.extend({}, [defaultOptions, options]);
 *
 * options будет иметь вид
 * {
 *     opt1: false,
 *     opt2: 'banana'
 * }
 * @author Anatoliy Yakimov
 */
Object.defineProperty(Object.prototype, 'extend', {
    value: function(obj, args) {
        args.forEach(function(arg) {
            Object.keys(arg).forEach(function(key) {
                if (arg[key] != null) {
                    obj[key] = arg[key];
                }
            });
        });
        return obj;
    }
});

/**
 * Аналог $.expand(true, obj, args) из jQuery.
 * Реекурсивно объединяет содержимое двух или более объектов в один объект.
 * Пример:
 * var defaultOptions = {
 *     opt1: {
 *         col1: 'red',
 *         col2: 'blue'
 *     }
 *     opt2: false
 * };
 * var options = {
 *     opt1: {
 *         col1: 'green'
 *         col3: 'MAGENTA'
 *     }
 * }
 * options = Object.extend({}, [defaultOptions, options]);
 *
 * options будет иметь вид
 * {
 *     opt1: {
 *         col1: 'green',
 *         col2: 'blue',
 *         col3: 'MAGENTA'
 *     }
 *     opt2: false
 * }
 * @author Anatoliy Yakimov
 */
Object.defineProperty(Object.prototype, 'extendDeep', {
    value: function(obj, args) {
        args = args.filter(function(arg) {
            return arg != null
        });
        args.forEach(function(arg) {
            Object.keys(arg).forEach(function(key) {
                if (arg[key] == null) {
                    return;
                }
                if (obj[key] != null && typeof obj[key] === 'object') {
                    if (Array.isArray(obj[key])) {
                        obj[key] = obj[key].concat(arg[key])
                    } else {
                        obj[key] = Object.extendDeep({}, [obj[key], arg[key]]);
                    }
                } else {
                    obj[key] = arg[key];
                }
            });
        });
        return obj;
    }
});
/**
 * Отображение окна ожидания загрузки
 */
function showLoader() {
    // modalOnce({
    //     headHTML: 'Пожалуйста подождите',
    //     bodyHTML: '<span style="color:black;">Выполняется операция</span><br /><img src="/img/3gloader.gif" alt="Данные загружаются" class="castor">',
    //     id: 'dataLoaderModal'
    // });
}
/**
 * Скрытие окна ожидания загрузки
 */
function hideLoader(modalName) {
    modalName = modalName ? modalName : 'dataLoaderModal';
    modalDelete(modalName);
}

/**
 * сведение параметров
 * @private
 * @param {Object} defaults параметр по умолчанию
 * @param {Object} options опции сведения
 */
var extend = function(defaults, options) {
    var extended = {};
    var prop;
    for (prop in defaults) {
        if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
            extended[prop] = defaults[prop];
        }
    }
    for (prop in options) {
        if (Object.prototype.hasOwnProperty.call(options, prop)) {
            extended[prop] = options[prop];
        }
    }
    return extended;
};

/**
 * Превращает url с query параметрами в пары ключ-значение
 * let params = getQueryParams('?id=1&name=Ivan');
 * console.log(params) //{ id: '1', name: 'Ivan'}
 * @author Anatoliy Yakimov
 */
function getQueryParams(url) {
    let queryParamsMap = {};
    let paramsStr = url.slice(url.search("\\?") + 1);
    paramsStr.split("&").forEach(function(pair) {
        let equalsPos = pair.search("=");
        let key = pair.slice(0, equalsPos);
        let value = pair.slice(equalsPos + 1);
        queryParamsMap[key] = value;
    });
    return queryParamsMap;
}
/**
 * Функция замены не безопасных символов на безопасные
 * @param {строка содержащая символы} string 
 * результат - строка с замененными символами
 */

function escape(string) {
    if (!string) return '';
    var htmlEscapes = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;'
    };
    return string.replace(/[&<>"']/g, function(match) {
        return htmlEscapes[match];
    });
}

/**
 * Функция очистки строки от лидирующих и замыкающих пробелов
 * @param {строка содержащая символы} str
 * результат - очищенная от пробелов строка
 */
function trim(str) {
    if (str == null) return null;
    while (str.indexOf(" ") == 0)
        str = str.substring(1, str.length);
    while (str.lastIndexOf(" ") == str.length - 1 && str != '')
        str = str.substring(0, str.length - 1);
    return str;
}

/**
 * Функция очистки строки от лидирующих и замыкающих пробелов
 * @param {текст с переводами строк} str
 * результат - очищенная от пробелов строка
 */
function trimText(str) {
    if (str == null) return null;
    while ((str.indexOf(" ") == 0) || (str.indexOf("\n") == 0) || (str.indexOf("\r") == 0))
        str = str.substring(1, str.length);
    while (((str.lastIndexOf(" ") == str.length - 1) ||
            (str.lastIndexOf("\n") == str.length - 1) ||
            (str.lastIndexOf("\r") == str.length - 1)) && str != '')
        str = str.substring(0, str.length - 1);
    return str;
}

/**
 * Функция возвращает пересечение массивов
 * @param {массив 1} a1
 * @param {массив 2} a2
 * @param {все последующие массив} ..rest
 * результат - массив, являющийся пересечением всех массивов, переданных в качестве параметров
 */
function intersect(a1, a2, rest) {
    const a12 = a1.filter(function(value){ return a2.includes(value) } )
    if (!rest || rest.length === 0)
        return a12;
    return intersect(a12, Array.from(rest));
};

/**
 * Функция возвращает форматированное представление для NSN
 * @param {оригинальный nsn} nsn
 * результат - строка, содержащая форматированное представление для NSN
 */
function formatNSN(nsn) {
    return nsn ? nsn.replace(/(.{4})(.{3})(.{4})/, '$1-$2-$3') : nsn;
}
/**
 * Функция возвращает GET параметры перекодированные
 * @author Vododokhov
 * @param имя параметра
 * результат - строка, содержащая форматированное параметры
 */
function extractGet(name) {
    var result = window.location.search.match(new RegExp(name + '=([^&=]+)'));
    return result ? decodeURIComponent(result[1].replace(/\+/g, " ")) : false;
}

/**
 * Включение/отключение подтверждения ухода со страницы
 * @param {boolean} enable
 */
function showConfirmOnCloseWindow(enable) {
    if (enable) {
        window.onbeforeunload = function() { return true; };
    } else {
        window.onbeforeunload = function() {};
    }
}

/**
 * Удаление фигурных скобок {}
 * - используется для приведение guid-идентификаторов к одному виду перед сравнением
 * @param string
 * @return {string}
 */
function removeCurlyBraces(string) {
    return (string == null) ? '' : string.replace('{', '').replace('}', '');
}

/**
 * Глобальная функция поиска по номеру документа
 * переадрисует на страницу с документами и выполнит поиск
 */
function searhDoc() {
    let vl = document.getElementById("srchTxt").value;
    window.location.href = "/tracing/document?vl=" + vl;
}
/**
 * Функция перевода даты в строку в формате гггг-мм-дд
 * @param {любая дата} date 
 */
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}
/**
 * Функция перевода даты из формата гггг-мм-дд в формат дд.мм.гггг
 * @param {любая строка} date 
 */
function formatDateMMDD(date) {
    let ar = date.split('-');

    if (ar[1].length < 2)
        ar[1] = '0' + ar[1];
    if (ar[2].length < 2)
        ar[2] = '0' + ar[2];

    return ar[2] + "." + ar[1] + "." + ar[0];
}
/**
 * Расшифровка типа документа
 * @param {*} code код типа
 * @returns 
 */
function dotTypetoRU(code) {

    return code;
}

/**
 * Модельное оно выбора типа шрифта (для редактора)
 */
function  showFontModal()
    {
        return;
        let tx=' <div class="input_row" style="display: contents;"> \
        <t-field class="select_field" style="display: inline-block;width: 185px;margin-right: 0px; margin-top: 2px;"> \
            <select id="fonselect2" title="Стиль шрифта"> \
                <option  selected value="Georgia, \'Times New Roman\', Times, serif" selected>Times New Roman</option> \
                <option value="Geneva, Arial, Helvetica, sans-serif">Arial</option> \
                <option value="Courier New, monospace">Courier New</option> \
                <option value="Comic Sans MS, Comic Sans, cursive">Comic Sans</option> \
            </select> \
            <div class="select__arrow fa fa-chevron-down"></div> \
        </t-field> \
        <t-field class="select_field" style="display: inline-block;width: 80px;    margin-top: 2px;"> \
            <select id="fonsize2" title="Размер шрифта" > \
                <option value="1">1</option> \
                <option value="2" selected>2</option> \
                <option value="3">3</option> \
                <option value="4">4</option> \
                <option value="5">5</option> \
                <option value="6">6</option> \
                <option value="7">7</option> \
            </select> \
            <div class="select__arrow fa fa-chevron-down"></div> \
        </t-field> \
    </div>'
        modalOnce({
        headHTML:'Настройка шрифтов',
        bodyHTML: tx,
        footerHTML:'<button class="buttonM darkcyan_50 blue" id="btn_font_upl">Применить</button>',
        class: 'madal_1х',
        id: 'font_window_modal'
    });
    document.getElementById("btn_font_upl").onclick=function()
    {
        act('fontName',$('#fonselect2').value);
        act('fontSize',$('#fonsize2').value);
        modalDeactivate('font_window_modal');
        modalHide('font_window_modal');
    }
    }


/**
 * Функция возвращает простой хеш код для строки
 * @returns ХЕШ код
 */
String.prototype.hashCode = function() {
    var hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
      chr   = this.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; 
    }
    return hash;

  };

/**
 * Функция блокирующая заданную кнопку
 */
function lockButtonTimeout(btn){
    btn.disabled = true; // блокирую кнопку
    window.setTimeout(function(){
        btn.disabled=false;}, 2000); // разблокирую кнопку через 2с

}


/**
 * Запись активного модуля в хранилеще
 * @author Vododokhov
 * @param  mod  код модуля
 * Пример использования:
 *     currentModule(&quot;' + item['mod'] + '&quot);
 */
function currentModule(mod) {
    //console.log('mod=', mod);
    deleteFromStorage('current_module');
    saveToLocalStorage('current_module', mod);
}
/**
 * Метод получения номера недели по указанной дате
 * нужен для построения граифков
 * @returns Номер недели
 */
Date.prototype.getWeek = function() {        
    var onejan = new Date(this.getFullYear(), 0, 1);        
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);   
 }