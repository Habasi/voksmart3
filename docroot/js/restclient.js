/**
 * Внутренняя обработка функции getData
 * @param ur - адрес маршрута
 * @param res - контейнер (структура) куда будут возвращены данные
 * @param {*} fname - калбек функция
 * @param {*} fooldata - признак возврата - по умолчанию в калбек функцию возвращается струкутра данных, иначе - полный ответ
 * @param {*} fnameError - калбек функция, при возникновении ошибки
 */
function _getData(ur, res, fname, fooldata, fnameError, method) {
    method = method ? method : 'GET';
    var xhr = new XMLHttpRequest();
    let error_txt = "";
    let v;
    if (vers) {
        xhr.open(method, ur, true);
        xhr.send();
        xhr.onreadystatechange = function (e) { //console.log(xhr);//console.log('Ответ: ', e, xhr);
            if (xhr.status == 204) {
                // console.log('Запрос выполняется, пожалуйста, подождите ...');
                return {}; // повторные запросы
            }
            if (xhr.readyState != 4) {
                // if(fnameError) fnameError(res);
                try {
                    let res_2 = JSON.parse(xhr.responseText);
                    if (res_2.status.error_code == 1) {
                        setError(res_2.status.error_message);
                    }
                } catch (e) { }
                return;
            }
            if (xhr.status != 200 && xhr.status != 501) {
                //&& xhr.status != 501

                if (xhr.status == 0 || xhr.status == "0")
                    error_txt = "Отсутствует связь с сервером! ";
                else if (xhr.status == 413) {
                    error_txt = "Размер вложения превышает допустимый предел!";
                    setError(error_txt, xhr.status, '_getDataError');
                    fnameError(res);
                }
                else
                    error_txt = "Ошибка обращения к серверу"; // "Ошибка обращения к серверу, код ошибки " + xhr.status;
                if (fnameError) {
                    fnameError(res);
                }
                else {
                    setError(error_txt, xhr.status, '_getDataError');
                }

            } else if (xhr.status == 501) {
                window.document.location.href = "/login.html";
            } else {
                try {
                    if (fooldata == 'html') { //console.log(xhr);
                        fname(xhr.response);
                        return 0;
                    }
                    var data = JSON.parse(xhr.responseText);
                    // произвольный json
                    if (fooldata == 'json') {
                        fname(data);
                        return 0;
                    }
                    //если статус не определен (файл например)
                    if (data.status==undefined || data.status.error_code == 0) {
                        if (!fooldata) {
                            res.data = data.data;
                            res.status = data.status;
                            res.result = true;
                            fname(res);
                        } else {
                            fname(data);
                        }
                    } else if (data.status.error_code == 501) {
                        window.document.location.href = "/login.html";
                    } else {
                        let tmx = "";
                        if (data.status.error_message_detail != "") {
                            tmx = data.status.error_message_detail + "<br />";
                        }
                        error_txt = "Ошибка: " + data.status.error_message + "<br />" + tmx;

                        if (fnameError) {
                            fnameError(res);
                        }
                        else {
                            setError(error_txt, '', '_getDataError2');
                        }
                    }
                } catch (e) {
                    console.log('e=', e);
                    let error_txt = "Ошибка получения данных " + e;
                    // throw e; // показать stack

                    if (fnameError) {
                        fnameError(res);
                    }
                    else {
                        setError(error_txt, xhr.status, '_getDataError1');
                    }
                }
            }
        }
    }
}

/**
 * Функция для получения JSON данных от сервера
 *
 * @param {*} ur - УРЛ, к которому надо обратиться
 * @param {*} res- объект дата результ по определенной структуре
 * @param {*} fname- имя функции, обработчика
 * @param {*} flData -передавать в функцию полный ответ (true) или только данные из ответа по умолчанию false
 * @param {*} fnameError - функция при ошибке
 */
function getData(ur, res, fname, flData, fnameError, method) {
    method = method ? method : 'GET';
    res = res ?
        res : {
            error_txt: null,
            data: null,
            result: true,
            isOk: function () { return this.result; }
        };

    let fooldata = false;
    if (flData === true) {
        fooldata = true;
    } else if (flData == 'json') {
        fooldata = 'json';
    } else if (flData == 'html') {
        fooldata = 'html';
    }
    if (!vers) {
        //проверим версию
        var xhr = new XMLHttpRequest();
        let v;
        //проверка версии
        xhr.open(method, '/api/versions', true);
        xhr.send();
        xhr.onreadystatechange = function (e) { //console.log('(version) Ответ: ', e, xhr);
            // console.log(xhr);
            if (xhr.status == 204) {
                // console.log('Запрос выполняется, пожалуйста, подождите ...');
                return {}; // повторные запросы
            }
            if (xhr.readyState != 4) {

                return false;
            }
            if (xhr.status != 200 && xhr.status != 501) {
                //   
                if (xhr.status == 0 || xhr.status == "0")
                    error_txt = "Отсутствует связь с сервером! ";
                else if (xhr.status == 413) {
                    error_txt = "Размер вложения превышает допустимый предел!";
                    fnameError(error_txt);
                }
                else
                    error_txt = ""; // "Ошибка обращения к серверу, код ошибки " + xhr.status;
                setError(error_txt, xhr.status, 'getDataError');
            } else if (xhr.status == 501) {
                window.document.location.href = "/login.html";
            } else {
                try {
                    var data = JSON.parse(xhr.responseText);
                    if (data.status.error_code == 0) {
                        v = data.data.server_version;
                        if (v == CURRENT_VERSION) {
                            vers = true;
                            _getData(ur, res, fname, fooldata, fnameError, method);
                        } else {
                            vers = false;
                            error_txt = "Ошибка контроля версии REST соглашения, <br /> текущая версия сервера   " + v + ", версия приложения " + CURRENT_VERSION;
                            setError(error_txt, '', 'getDataErrorV');
                        }
                    } else {
                        vers = false;
                        error_txt = "Ошибка  " + data.status.error_message + "<br/>" + data.status.error_message_detail;
                        setError(error_txt, '', 'getDataError2');
                    }
                } catch (e) {
                    vers = false;
                    console.log(e);
                    error_txt = "Ошибка получения данных " + e;
                    setError(error_txt, xhr.status, 'getDataError1');
                }
            }
        }
    } else {
        _getData(ur, res, fname, fooldata, fnameError, method);
    }
}

function setError(txt){
    console.error(txt);
}

/*
    Функция обновления данных
    параметры:
    doc - объект который должен отправиться на сервер
    ur - УРЛ, к которому надо обратиться
    funmane - имя функции, обработчика, если не нужна то ''
    
*/
function updData(url, doc, funmane, sync) {
    // let timerId=setTimeout(showLoader,900);
    sync = sync != undefined ? sync : true;
    var data = JSON.stringify(doc);
    var xhr = new XMLHttpRequest();
    var res = [];
    xhr.open("PUT", url, sync);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // clearTimeout(timerId);
            hideLoader();
            try {
                var json = JSON.parse(xhr.responseText);
                if (json.status.error_code == 0) {
                    if (funmane && typeof(funmane) == 'function') funmane(json);
                } else {
                    // clearTimeout(timerId);
                    hideLoader();
                    let tmx = "";
                    if (json.status.error_message_detail != "") {
                        tmx = json.status.error_message_detail.replace(/\n/g, "<br />") + "<br />";
                    }
                    let error_txt = "Ошибка: " + json.status.error_message.replace(/\n/g, "<br />") + "<br />" + tmx;
                    setError(error_txt, '', 'updDataError');
                }
            } catch (err) {
                // clearTimeout(timerId);
                hideLoader();
                let error_txt = "Ошибка разбора полученных данных! " + err;
                setError(error_txt, xhr.status, 'updDataError2');
                console.log(err);
            }
        } else if (xhr.readyState != 1 && xhr.readyState != 0 && xhr.status != 200) {
            if (xhr.status == 204) {
                // console.log('Запрос выполняется, пожалуйста, подождите ...');
                showLoader();
                return {}; // повторные запросы
            }
            // clearTimeout(timerId);
            hideLoader();
            if (xhr.status == 413) {
                hideLoader('_getDataError');
                setError("Размер вложения превышает допустимый предел!", xhr.status, '_getDataError');
                return;
            }
            let error_txt = "Ошибка получения данных!";
            setError(error_txt, xhr.status, 'updDataError1');
        }
    };
    try {
        xhr.send(data);
    } catch (err) {
        // clearTimeout(timerId);
        hideLoader();
        let error_txt = "Ошибка отправки запроса! " + err;
        setError(error_txt, 'updDataError');
    }
    try {
        if (sync == false) {
            res = JSON.parse(xhr.response) || [];
        }
    } catch (e) {
        // clearTimeout(timerId);
        hideLoader();
        console.log(e);
    }

    return res;
}

/*
    Функция добавления данных
    параметры:
    
    ur - УРЛ, к которому надо обратиться
    dt - объект который должен отправиться на сервер
    funmane - имя функции, обработчика, если не нужна то ''
    
*/
function saveData(ur, dt, funmane, err_funct) {
    let timerId = setTimeout(showLoader, 900);
    var data = JSON.stringify(dt);
    var xhr = new XMLHttpRequest();
    var url = ur;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        // console.log(xhr);
        if (xhr.status == 204) {
            // console.log('Запрос выполняется, пожалуйста, подождите ...');
            return {}; // повторные запросы
        }
        if (xhr.readyState == 4 && xhr.status == 200) {
            clearTimeout(timerId);
            hideLoader();
            try {
                var json = JSON.parse(xhr.responseText);
                if (json.status.error_code == 0) {
                    if (funmane && typeof(funmane) == 'function') funmane(json);
                } else {
                    clearTimeout(timerId);
                    hideLoader();
                    let tmx = ""; //="";
                    console.log(json.status.error_code, json.status.error_message);
                    if (json.status.error_message_detail != "") {
                        tmx = json.status.error_message_detail.replace(/\n/g, "<br />") + "<br />";
                    }
                    let error_txt = "Ошибка: " + json.status.error_message + "<br />" + tmx;
                    setError(error_txt, '', 'saveDataError');
                }
            } catch (err) {
                clearTimeout(timerId);
                hideLoader();
                let error_txt = "Ошибка разбора полученных данных! " + err;
                setError(error_txt, xhr.status, 'saveDataError2');
                console.log(err);
            }
        } else if (xhr.status != 200 && xhr.status > 0) {
            clearTimeout(timerId);
            hideLoader();
            if (xhr.status == 413) {
                hideLoader('_getDataError');
                setError("Размер вложения превышает допустимый предел!", xhr.status, '_getDataError');
                return;
            }
            let error_txt = "Ошибка получения данных! ";
            setError(error_txt, xhr.status, 'saveDataError1');
        }
    };
    try {
        xhr.send(data);
    } catch (err) {
        clearTimeout(timerId);
        hideLoader();
        let error_txt = "Ошибка отправки запроса! " + err;
        setError(error_txt, '', 'saveDataError');
    }
}

/*
    Функция удаления данных
    параметры:
    
    ur - УРЛ, к которому надо обратиться
    dt - объект который должен отправиться на сервер
    funmane - имя функции, обработчика, если не нужна то ''
    
*/
function delData(ur, dt, funmane) {
    let timerId = setTimeout(showLoader, 900);
    var data = JSON.stringify(dt);
    var xhr = new XMLHttpRequest();
    var url = ur;;
    xhr.open("delete", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.status == 204) return; // повторные запросы
        if (xhr.readyState == 4 && xhr.status == 200) {
            clearTimeout(timerId);
            hideLoader();
            try {
                var json = JSON.parse(xhr.responseText);
                if (json.status.error_code == 0) {
                    if (funmane && typeof(funmane) == 'function') funmane(json);
                } else {
                    let error_txt = "Ошибка: " + json.status.error_message + "<br/>" + json.status.error_message_detail;
                    setError(error_txt, '', 'delDataError');
                }
            } catch (err) {
                let error_txt = "Ошибка разбора полученных данных! " + err;
                setError(error_txt, xhr.status, 'delDataError2');
            }
        } else if (xhr.readyState != 1 && xhr.readyState != 0 && xhr.status != 200) {
            clearTimeout(timerId);
            hideLoader();
            if (xhr.status == 413) {
                hideLoader('_getDataError');
                setError("Размер вложения превышает допустимый предел!", xhr.status, '_getDataError');
                return;
            }
            let error_txt = "Ошибка получения данных! ";
            setError(error_txt, xhr.status, 'delDataError1');
        }
    };
    try {
        xhr.send(data);
    } catch (err) {
        clearTimeout(timerId);
        hideLoader();
        let error_txt = "Ошибка отправки запроса! " + err;
        setError(error_txt, '', 'delDataError');
    }
}


function post(api, params, collback){
    collback = collback || function(){};
    params = params || {};
    var xhr = new XMLHttpRequest();

    var body = `arguments=${encodeURIComponent(JSON.stringify(params))}`;
    // console.log(body);

    xhr.open("POST", api, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function(e){
        if(xhr.readyState == 4 && e.target.response){
            let json;
            try {
                json = JSON.parse(e.target.response);
            } catch(u) {
                json = {};
                setError(e.target.response);
            }
            collback(json);
        }
    }
    xhr.send(body);
}

function postBlob(api, blob, collback){
    var data = new FormData();
    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/test-file', true);
    xhr.onload = function (e) {
        // console.log(e.target.response);
      
        if(xhr.readyState == 4 && e.target.response){
            let json;
            try {
                json = JSON.parse(e.target.response);
            } catch(u) {
                json = {};
                setError(e.target.response);
            }
            collback(json);
        }
    };
    // var blob = new Blob(['abc123'], {type: 'text/plain'});
    data.append('file', blob);
    // console.log(blob.text());
    xhr.send(data);
}

function bin2String(array) {
    var result = "";
    for (var i = 0; i < array.length; i++) {
      result += String.fromCharCode(parseInt(array[i], 2));
    }
    return result;
}
function string2Bin(str) {
    var result = [];
    for (var i = 0; i < str.length; i++) {
      result.push(str.charCodeAt(i).toString(2));
    }
    return result;
  }