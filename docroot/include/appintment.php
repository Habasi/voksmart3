<section class="ftco-section ftco-no-pt ftco-margin-top" style="margin-top: 7em;" id="section-appointment">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="request-quote">
                    <div class="bg-primary py-4">
                        <span class="subheading">Оставить заявку</span>
                        <h3>Наш инженер с вами свяжется в ближайшие 2 часа!</h3>
                    </div>
                    <form action="#" class="request-form ftco-animate">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Имя">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Почта">
                        </div>
                        <div class="form-group">
                            <div class="form-field">
                                <div class="select-wrap">
                                    <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                    <select name="" id="" class="form-control">
                                        <option value="">Тип работ</option>
                                        <option value="">Монтаж отопления</option>
                                        <option value="">Монтаж кондиционерования</option>
                                        <option value="">Установка промышенной системы холодоснабжения </option>
                                        <option value="">Монтаж пожаротушения</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Телефон">
                        </div>
                        <div class="form-group">
                            <textarea name="" id="" cols="30" rows="2" class="form-control" placeholder="Комментарий"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Отправить" class="btn btn-primary py-3 px-4">
                        </div>
                    </form>
                </div>
            </div>
            <!-- <div class="col-xl-1 d-xl-block d-none"></div> -->
            <div class="col-md-8 col-lg-8 col-xl-8">
                <div class="heading-section ftco-animate mb-5">
                    <!-- <span class="subheading">Testimonials</span> -->
                    <h2 class="mb-4">Наши преимущества</h2>
                    <!-- <p>...</p> -->
                </div>
                <div class="carousel-testimony owl-carousel" id="advantages">
                    <div class="item">
                        <div class="testimony-wrap">
                            <div class="text bg-light p-4">
                                <span class="quote d-flex align-items-center justify-content-center">
                                    <i class="icon-quote-left"></i>
                                </span>
                                <p>ВокСмарт работает на рынке уже более 15 лет и обладает высококвалифицированными специалистами и богатым опытом в выполнении различных строительных и монтажных работ. Благодаря чему, мы можем решать самые сложные и нетипичные задачи.</p>
                                <p class="name">Профессионализм и опыт</p>
                                <span class="position">Farmer</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap">
                            <div class="text bg-light p-4">
                                <span class="quote d-flex align-items-center justify-content-center">
                                    <i class="icon-quote-left"></i>
                                </span>
                                <p>Компания ВокСмарт стремится к высокому качеству выполнения работ. Это включает в себя правильный выбор оборудования, его профессиональную установку и тщательную настройку системы для обеспечения оптимальной производительности.</p>
                                <p class="name">Высокое качество работы</p>
                                <span class="position">Businessman</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap">
                            <div class="text bg-light p-4">
                                <span class="quote d-flex align-items-center justify-content-center">
                                    <i class="icon-quote-left"></i>
                                </span>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <p class="name">Mark Huff</p>
                                <span class="position">Students</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap">
                            <div class="text bg-light p-4">
                                <span class="quote d-flex align-items-center justify-content-center">
                                    <i class="icon-quote-left"></i>
                                </span>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <p class="name">Rodel Golez</p>
                                <span class="position">Striper</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap">
                            <div class="text bg-light p-4">
                                <span class="quote d-flex align-items-center justify-content-center">
                                    <i class="icon-quote-left"></i>
                                </span>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <p class="name">Ken Bosh</p>
                                <span class="position">Manager</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        let advantages = [
            {
                name: 'Профессионализм и опыт',
                description: 'ВокСмарт работает на рынке уже более 15 лет и  обладает  высококвалифицированными специалистами и богатым опытом в выполнении различных строительных и монтажных работ. Благодаря чему, мы можем решать самые сложные и нетипичные задачи.'
            },
            {
                name: 'Высокое качество работы',
                description: 'Компания ВокСмарт стремится к высокому качеству выполнения работ. Это включает в себя правильный выбор оборудования, его профессиональную установку и тщательную настройку системы для обеспечения оптимальной производительности.'
            },
            {
                name: 'Соблюдение стандартов и норм',
                description: 'Мы тщательно следим за соблюдением всех необходимых стандартов и норм в области вентиляции и кондиционирования. Это обеспечивает безопасность и эффективность систем.'
            },
            {
                name: 'Надежность',
                description: 'Клиенты могут спокойно погалаться на нашу команду, так как выполняет свою работу качественно и в срок. Мы также предоставляем гарантию на наши работы от 2 лет.'
            },
            {
                name: 'Снижение затрат на обслуживание',
                description: 'Эффективные и качественные системы вентиляции и кондиционирования требуют меньше обслуживания и ремонта в будущем, что снижает операционные затраты.'
            },
            {
                name: 'Сокращение издержек на оборудование',
                description: 'Компания ВокСмарт имеет доступ к оптовым ценам на оборудование и материалы, что позволяет заказчикам сэкономить средства при закупке необходимых компонентов.'
            },
            {
                name: 'Сервис и техническая поддержка',
                description: 'ВокСмарт предоставляет клиентам сервис и техническую поддержку даже после завершения проекта, что обеспечивает долгосрочную работоспособность системы.'
            },
            {
                name: 'Соблюдение сроков',
                description: 'Мы обязательно соблюдают сроки выполнения работ. Это позволяет заказчикам планировать свои проекты и операции с минимальными задержками.'
            },
            {
                name: 'Индивидуальный подход',
                description: 'Каждый проект требует индивидуального подхода, и мы готовы адаптировать свои услуги под конкретные потребности заказчика.'
            },
            {
                name: 'Репутация и доверие',
                description: 'ВокСмарт обладает хорошей репутацией и многолетним опытом обычно наследуют доверие клиентов и партнеров, что способствует долгосрочным отношениям и успешным проектам.'
            },
            {
                name: 'Гарантии и сертификации',
                description: 'ВокСмарт предоставляет гарантии на свои работы и могут быть сертифицированы, что подтверждает их профессионализм и качество услуг.'
            },
            {
                name: 'Безопасность',
                description: 'Монтажные компании строго соблюдают нормы и стандарты безопасности на стройплощадке, что уменьшает риски происшествий и несчастных случаев.'
            },
        ];
        document.querySelector('#advantages').innerHTML = advantages.map(function(item) {
            return `
                <div class="item">
                    <div class="testimony-wrap">
                        <div class="text bg-light p-4">
                            <span class="quote d-flex align-items-center justify-content-center">
                                <i class="icon-quote-left"></i>
                            </span>
                            <p>${item.description}</p>
                            <p class="name"><b>${item.name}</b></p>
                            <!--<span class="position">Businessman</span>-->
                        </div>
                    </div>
                </div>
                `;
        }).join('');
    </script>
</section>