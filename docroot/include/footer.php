<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2 logo"><span class=""></span>ВОКСМАРТ</h2>
                    <p>
                        ИНН: 7702379600<br>
                        КПП: 771501001<br>
                        ОГРН: 157746259384<br>
                        Юридический адрес: 127106, г. Москва, ул. Гостиничная, д. 3, эт. 5 оф. 513
                    </p>
                </div>

            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-5 ml-md-4">
                    <h2 class="ftco-heading-2">Услуги</h2>
                    <ul class="list-unstyled">
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Монтаж вентиляции</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Монтаж отопления</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Монтаж кондиционирование</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Установка промышенной системы холодоснабжения</a></li>
                        <li><a href="#"><span class="ion-ios-arrow-round-forward mr-2"></span>Монтаж пожаротушения</a></li>
                    </ul>
                </div>
            </div>
            <!-- <div class="col-md-5">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2">Каталог</h2>
                    <div class="block-21 mb-4 d-flex">
                        <a class="blog-img mr-4" style="background-image: url(/docroot/images/image_1.jpg);"></a>
                        <div class="text">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> Feb. 07, 2018</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 mb-5 d-flex">
                        <a class="blog-img mr-4" style="background-image: url(/docroot/images/image_2.jpg);"></a>
                        <div class="text">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> Feb. 07, 2018</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="col-md">
                <div class="ftco-footer-widget mb-5">
                    <h2 class="ftco-heading-2">Контактная информация</h2>
                    <p>
                        Часы работы компании:<br>
                        Пн.-Пт. С 9:00 - 18:00 (МСК) <br>
                        Телефон: 8 (905) 716 - 04 - 06<br>
                        Почта: rdv-eng@mail.ru
                    </p>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
                        <li class="ftco-animate"><a href="#"><span class="fa fa-whatsapp"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="fa fa-envelope"></span></a></li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    &copy;2023 Воксмарт | Made with <i class="icon-heart" aria-hidden="true"></i> <a href="" target="_blank">Habasi</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>