<section class="ftco-counter img" id="section-counter" style="background-image: url(/docroot/images/bg_3.jpg);" data-stellar-background-ratio="0.5">
    <div class="container">
        <h1 class="mb-4">Цифры и рейтинги "ВОКсмарт</h1>
        <div class="row">
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 d-flex">
                    <div class="text d-flex align-items-center">
                        <strong class="number" data-number="15">0</strong><strong>+</strong>
                    </div>
                    <div class="text-2">
                        <span><br><br><br>Лет практики</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 d-flex">
                    <div class="text d-flex align-items-center">
                    <strong>></strong><strong class="number" data-number="40">0</strong>
                    </div>
                    <div class="text-2">
                        <span><br><br>Выполненых проектов</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 d-flex">
                    <div class="text d-flex align-items-center">
                        <strong class="number" data-number="5">0</strong><strong>+</strong>
                    </div>
                    <div class="text-2">
                        <span><br><br><br>Лет гарантии</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 d-flex">
                    <div class="text d-flex align-items-center">
                        <strong class="number" data-number="95">0</strong><strong>%</strong>
                    </div>
                    <div class="text-2">
                        <span>клиентов становятся нашей постоянной клиентской базой</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>