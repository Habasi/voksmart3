<!-- <section class="ftco-intro" style="background-image: url(/docroot/images/home-slider/home-slider-4.jpg);" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h2></h2>
                <p>"ВокСмарт" - это компания с богатым опытом и высокой репутацией в сфере строительства и обслуживания инженерных систем. Уже более 16 лет мы специализируемся на предоставлении комплексных решений в сфере производства санитарно-технических работ, монтажных отопительных систем и систем кондициоривания воздуха.
Наша команда специалистов готова преобразовать вашу идею в реальность, обеспечив эффективность, надежность и выское качество работ.
Задачами, которыми мы занимаемся.
Создаем максимально комфортные климатические условия в вашем помещении с использованием современных систем кондиционирования. Наши эксперты помогут вам выбрать оптимальные решения для вашего проекта и обеспечат их монтаж и обслуживание.
Разрабатываем эффективные системы отопления, которые обеспечивают равномерное и энергосберегающее распределение тепла. Наши решения включают в себя установку современных котлов, радиаторов и системы теплоснабжения.
Создаем системы вентиляции, которые обеспечивают постоянное обновление воздуха в помещении и поддерживают оптимальные условия для здоровья и комфорта. Наши специалисты проводят проектирование и монтаж системы вентиляции, учитывая специфику вашего объекта.
Мы заботимся о безопасности в случае пожара. Наши системы дымоудаления обеспечивают эффективное удаление дыма и газов, что способствует эвакуации и спасению жизней.</p>
                <p class="mb-0">
                    <a href="#" class="btn btn-primary px-4 py-3">Предоставить коммерческое предложение</a>
                    <a href="#" class="btn btn-primary px-4 py-3">Консультация инженера</a>
                </p>
            </div>
        </div>
    </div>
</section> -->
<section class="home-slider owl-carousel" id="home-slider-2" style="background-image: url('/docroot/images/home-slider/home-slider-4.jpg'); background-repeat: no-repeat; background-position: center; background-size: cover;">
    <script>
        let slide2 = [
                { image:'/docroot/images/home-slider/home-slider-4.jpg', name:`<? apchi('intro-1'); ?>` },
            ];
            document.querySelector('#home-slider-2').innerHTML = slide2.map(function(item){
                return `
                <div class="slider-item" style="background-image:url(${item.image}); background-position: center !important;" data-stellar-background-ratio="0.5">
                    <div class="overlay" style=""></div>
                    <div class="container">
                        <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
                            <div class="col-md-6 text ftco-animate pl-md-5">
                            <!--<h1 class="mb-4 text-dark">${item.name}</h1>-->
                                <h3 class="subheading">${item.name}</h3>
                            </div>
                        </div>
                    </div>
                </div>
                `;
            }).join('');
    </script>
</section>