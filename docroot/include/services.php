<section class="ftco-services ftco-no-pt" id="section-services">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
            <div class="col-md-8 text-center heading-section ftco-animate">
                <!-- <span class="subheading">Услуги</span> -->
                <h2 class="mb-4">Наши услуги</h2>
                <!-- <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> -->
            </div>
        </div>
        <div class="row" id="services"></div>
    </div>

    <script>
        
        let services = [
            {
                name: '<? apchi('servis-name-1'); ?>',
                icon: 'flaticon-hook',
                description: '<? apchi('servis-description-1'); ?>'
            },
            {
                name: '<? apchi('servis-name-2'); ?>',
                icon: 'flaticon-skyline',
                description: '<? apchi('servis-description-2'); ?>'
            },
            {
                name: '<? apchi('servis-name-3'); ?>',
                icon: 'flaticon-stairs',
                description: '<? apchi('servis-description-3'); ?>'
            },
            {
                name: '<? apchi('servis-name-4'); ?>',
                icon: 'flaticon-home',
                description: '<? apchi('servis-description-4'); ?>'
            },
            {
                name: '<? apchi('servis-name-5'); ?>',
                icon: 'flaticon-poz',
                description: '<? apchi('servis-description-5'); ?>'
            },
            {
                name: '<? apchi('servis-name-6'); ?>',
                icon: 'flaticon-montaz',
                description: '<? apchi('servis-description-6'); ?>'
            },
        ];
        document.querySelector('#services').innerHTML = services.map(function(item, key) {
            return `
                <div class="col-md-4 d-flex services align-self-stretch p-4 ftco-animate">
                    <div class="media block-6 d-block text-center" style="margin:auto;">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="${item.icon}"></span>
                        </div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading">${item.name}</h3>
                            <p>
                                ${item.description}
                                <!--<a href="#section-services" onclick="window.dialog_${key}.showModal();">Подробнее</a>-->
                            </p>
                        </div>
                    </div>
                </div>

                <dialog id="dialog_${key}">
                	<h2>${item.name}</h2>
                	<p>${item.description}</p>
                	<button onclick="window.dialog_${key}.close();" aria-label="close" class="x">❌</button>
                </dialog>
                `;
        }).join('');
    </script>
</section>