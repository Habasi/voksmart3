<section class="ftco-section ftco-no-pt ftco-no-pb" id="section-projects">
    <div class="container-fluid p-0">
        <div class="row no-gutters justify-content-center mb-5 pb-2" id="projects-head">
            <div class="col-md-6 text-center heading-section ftco-animate">
                <!-- <span class="subheading">Проекты</span> -->
                <h2 class="mb-4">Выполненные работы</h2>
                <!-- <p>...</p> -->
            </div>
        </div>
        <div class="row no-gutters" id="projects">
        </div>
    </div>
    <script>
        let projects = [
            {
                object: 'УК СПАРТАКОВСКИЙ 2',
                addres: 'г. Москва, Спартаковский переулок, дом 2',
                type: 'монтаж и установка центрального кондиционирования, установка спринклерной системы пожаротушения.',
                image: '/docroot/images/projects/5.jpg',
            },
            {
                object: 'АО Мебельная компания «Шатура»',
                addres: 'Московская область, г. Шатура, Ботанический проезд, дом 37',
                type: 'монтаж и установка вентиляционных систем и кондиционирования.',
                image: '/docroot/images/projects/6.jpg',
            },
            {
                object: 'МИЦ «Хорошевская»',
                addres: 'г. Москва, 3-я Хорошевская, вл. 19, стр 3',
                type: 'монтаж и установка вентиляционных систем и кондиционирования, установка спринклерной системы пожаротушения.',
                image: '/docroot/images/projects/7.png',
            },
            {
                object: 'МИЦ «Детский садик»',
                addres: 'г. Москва, поселение Доссеновское, ш. Калужское, вл. 52Б, 35 км',
                type: 'монтаж и установка вентиляционных систем и кондиционирования, проведение пусконаладочных работ.',
                image: '/docroot/images/projects/8.jpg',
            },
            {
                object: 'ООО «ГЕЙМ ШОУ ГРУПП»',
                addres: 'г. Москва, ул. Смольная, дом 12',
                type: 'монтаж тепловых пунктов системы отопления.',
                image: '/docroot/images/projects/9.jpg',
            },
            {
                object: 'АО «ТДМ»',
                addres: 'Ярославкая область, г. Рыбинск, Ярославский тракт, 62',
                type: 'монтаж и установка вентиляционных систем и кондиционирования, дымоудаления и отопления.',
                image: '/docroot/images/projects/10.png',
            },
            {
                object: 'ООО МЗ «Тонар»',
                addres: 'Московская область, город Орехово-Зуево, д. Губино, ул. Ленинская 1- я, дом 76а',
                type: 'монтаж и установка вентиляционных систем и кондиционирования, дымоудаления и отопления.',
                image: '/docroot/images/projects/11.jpg',
            },
        ];
        document.querySelector('#projects').innerHTML = projects.map(function(item, key){
            return `
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="project" onclick="window.dialog_project_${key}.showModal();" style="cursor:pointer;">
                    <img src="${item.image}" class="img-fluid" alt="Colorlib Template" style="width:100%; height: 300px;">
                    <div class="text">
                        <!--<span><a style="cursor:pointer;" onclick="window.dialog_project_${key}.showModal();">Подробнее</a></span>-->
                        <h3><a style="cursor:pointer;" onclick="window.dialog_project_${key}.showModal();">${item.object}</a></h3>
                    </div>
                    <!--<a href="${item.image}" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="icon-expand"></span>
                    </a>-->
                </div>
            </div>
            
            <dialog id="dialog_project_${key}">
            	<h2>${item.object}</h2>
                <img src="${item.image}" class="img-fluid" alt="Colorlib Template" style="width:100%; height: 300px;">
                <br><br>
            	<p>
                    <mark>Тип работ</mark>: ${item.type}
                    <footer class="blockquote-footer">${item.addres}</footer>
                </p>
            	<button onclick="window.dialog_project_${key}.close();" aria-label="close" class="x">❌</button>
            </dialog>
            `;
        }).join('');

        if(document.location.pathname == '/projects'){
            document.querySelector('#projects-head').style.display = 'none';
        }
    </script>
</section>