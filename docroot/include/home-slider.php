<section class="home-slider owl-carousel" id="home-slider">
    <script>
        let slide = [
                { image:'/docroot/images/home-slider/home-slider-1.jpg', name:'<? apchi('home-intro-1'); ?>' },
                { image:'/docroot/images/home-slider/home-slider-2.webp', name:'<? apchi('home-intro-1'); ?>' },
                { image:'/docroot/images/home-slider/home-slider-3.jpg', name:'<? apchi('home-intro-1'); ?>' },
            ];
            document.querySelector('#home-slider').innerHTML = slide.map(function(item){
                return `
                <div class="slider-item" style="background-image:url(${item.image});" data-stellar-background-ratio="0.5">
                    <div class="overlay" style="left:0;"></div>
                    <div class="container">
                        <div class="row no-gutters slider-text align-items-center /*justify-content-end*/" data-scrollax-parent="true">
                            <div class="col-md-6 text ftco-animate">
                                <h1 class="mb-4 text-dark">${item.name}</h1>
                                <!--<h3 class="subheading">${item.name}</h3>-->
                                <p>
                                    <button href="#" class="btn btn-primary px-4 py-3 mt-3">Рассчитать стоимость</button>
                                    <button href="#" class="btn btn-primary px-4 py-3 mt-3">Консультация инженера</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                `;
            }).join('');
    </script>
</section>