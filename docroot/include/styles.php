<!-- <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900" rel="stylesheet"> -->

<link rel="stylesheet" href="/docroot/css/open-iconic-bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/docroot/css/animate.css">

<link rel="stylesheet" href="/docroot/css/owl.carousel.min.css">
<link rel="stylesheet" href="/docroot/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/docroot/css/magnific-popup.css">

<link rel="stylesheet" href="/docroot/css/aos.css">

<link rel="stylesheet" href="/docroot/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="/docroot/css/jquery.timepicker.css">

<link rel="stylesheet" href="/docroot/css/style.css">

<link rel="stylesheet" href="/docroot/css/swiper.css">

<link rel="stylesheet" href="/docroot/fonts/flaticon/flaticon.css">
<!-- <link rel="stylesheet" href="/docroot/fonts/gunnyrewritten/gunnyrewritten.css"> -->
<!-- <link rel="stylesheet" href="/docroot/fonts/engine/engine.css"> -->
<!-- <link rel="stylesheet" href="/docroot/fonts/steamy/steamy.css"> -->
<!-- <link rel="stylesheet" href="/docroot/fonts/corkituscan/corkituscan.css"> -->
<link rel="stylesheet" href="/docroot/fonts/reef/reef.css">
<!-- <link rel="stylesheet" href="/docroot/fonts/gooseberry/gooseberry.css"> -->
<!-- <link rel="stylesheet" href="/docroot/fonts/fyodorexpanded/fyodorexpanded.css">
<link rel="stylesheet" href="/docroot/fonts/opengosta/opengosta.css"> -->
<link href="/docroot/css/itc-slider.css" rel="stylesheet">
<style id="scheme-css">
@import url('https://fonts.googleapis.com/css2?family=Merriweather:ital@1&family=Oswald:wght@500&display=swap');
@import url('https://pro.fontawesome.com/releases/v6.0.0-beta1/css/all.css');

/* grid layout */
ol#scheme, ol#scheme::before, ol#scheme::after, 
ol#scheme *, ol#scheme *::before, ol#scheme *::after { margin: 0; padding: 0; box-sizing: border-box }
ol#scheme { 
  display: inline-grid;
  grid-template-columns: repeat(auto-fit, minmax(15rem, 1fr));
  gap: 1rem;
  
  width: min(60rem, calc(100% - 2rem));
  margin-inline: auto;
  padding-block: 1rem;
  
  list-style: none;
  counter-reset: count;
}

/* card layout/styling */
ol#scheme > li {
  --card-background-color: #f0f0f0;
  --card-text-color: #0F0F0F;
  --card-border-radius: 0.5rem;
  --card-padding-block: 1.5rem;
  --card-padding-inline: 1rem;
  
  --outset-size: 0.75rem;
  --outset-background-color: #e5e5e5;
  
  --number-font-size: 3rem;
  --number-overlap: 0.5rem;
  --number-font-family: 'Oswald', sans-serif;
  --number-font-weight: 500;
  
  margin: var(--outset-size);
  margin-top: calc(var(--number-font-size) - var(--number-overlap));
  border-radius: var(--card-border-radius);
  padding-block: var(--card-padding-block);
  padding-inline: var(--card-padding-inline);
  
  color: var(--card-text-color);
  background-color: var(--card-background-color);
  box-shadow: 
    inset 1px 1px 0px rgb(255 255 255 / .5),
    inset -1px -1px 0px rgb(0 0 0 / .25),
    calc(var(--outset-size) * .25) calc(var(--outset-size) * .25) calc(var(--outset-size) * .5) rgb(0 0 0 / .25);
  position: relative;
  counter-increment: count;

  height: 400px;
}

ol#scheme > li::after{
  content: counter(count, decimal-leading-zero);
  position: absolute;
  
  bottom: calc(100% - var(--number-overlap));
  left: 50%;
  transform: translateX(-50%);
  
  color: var(--accent-color);
  font-family: var(--number-font-family);
  font-weight: var(--number-font-weight);
  font-size: var(--number-font-size);
  line-height: 1.5;
  z-index: -1;
}
ol > li::before{
  content: "";
  position: absolute;
  width: calc(100% + (var(--outset-size) * 2));
  height: 100%;
  bottom: calc(var(--outset-size) * -1);
  left: calc(var(--outset-size) * -1);
  z-index: -1;
  
  border-bottom-left-radius: calc(var(--card-border-radius) + var(--outset-size));
  border-bottom-right-radius: calc(var(--card-border-radius) + var(--outset-size));
  
  background-color: var(--outset-background-color);
  
  background-image: 
    linear-gradient(to left, var(--outset-background-color) calc(var(--outset-size) * 2), transparent 0),
    linear-gradient(135deg, var(--accent-color) 80%, var(--outset-background-color) 0);    
}

/* card content */
#scheme h1 {
  font-size: 2.5rem;
  font-family: 'Oswald', sans-serif;
  text-align: center;
  font-weight: normal
}
#scheme .icon{
  font-size: 2rem;
  text-align: center;
  margin-bottom: calc(var(--card-padding-block) * .5);
}
#scheme .title {
  text-transform: uppercase;
  font-family: 'Oswald', sans-serif;
  text-align: center;
  color: var(--accent-color)
}
#scheme .descr {
  color: var(--text-color);
  font-size: 0.75rem;
  font-family: 'Merriweather', serif;
  text-align: center;
}

/*  */
/* body {
  background-color: #f5f5f5;
  min-height: 100vh;
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
} */


    .itc-slider-item {
      flex: 0 0 33.3333333333%;
      max-width: 33.3333333333%;
      /* height: 250px; */
      display: flex;
      justify-content: center;
      align-items: center;
      color: rgba(255,255,255, 0.8);
      /* font-size: 7rem; */
    }
  
</style>
<style id="projects-css">
  
</style>