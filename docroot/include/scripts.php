<script src="/docroot/js/jquery.min.js"></script>
<script src="/docroot/js/jquery-migrate-3.0.1.min.js"></script>
<script src="/docroot/js/popper.min.js"></script>
<script src="/docroot/js/bootstrap.min.js"></script>
<script src="/docroot/js/jquery.easing.1.3.js"></script>
<script src="/docroot/js/jquery.waypoints.min.js"></script>
<script src="/docroot/js/jquery.stellar.min.js"></script>
<script src="/docroot/js/owl.carousel.min.js"></script>
<script src="/docroot/js/jquery.magnific-popup.min.js"></script>
<script src="/docroot/js/aos.js"></script>
<script src="/docroot/js/jquery.animateNumber.min.js"></script>
<script src="/docroot/js/bootstrap-datepicker.js"></script>
<script src="/docroot/js/jquery.timepicker.min.js"></script>
<script src="/docroot/js/scrollax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<!-- <script src="/docroot/js/google-map.js"></script> -->
<script src="/docroot/js/swiper.js"></script>
<script src="/docroot/js/main.js"></script>
<script src="/docroot/js/itc-slider.js" defer=""></script>
<script src="/docroot/js/restclient.js" defer=""></script>
<script src="/docroot/js/utils.js" defer=""></script>