<div class="bg-top navbar-light">
	<div class="container">
		<div class="row no-gutters d-flex align-items-center align-items-stretch">
			<div class="col-md-4 d-flex align-items-center py-4">
				<a class="navbar-brand" href="/" style="font-family: 'Reef';"><span class="mr-1"></span>ВОКСМАРТ</a>
			</div>
			<div class="col-lg-8 d-block">
				<div class="row d-flex">
					<div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
						<div class="icon d-flex justify-content-center align-items-center"><span class=""><i class="fa fa-clock-o"></i></span></div>
						<div class="text d-flex align-items-center">
							<span>с 9:00 до 18:00</span>
						</div>
					</div>
					<div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
						<div class="icon d-flex justify-content-center align-items-center"><span class=""><i class="fa fa-envelope"></i></span></div>
						<div class="text d-flex align-items-center">
							<span>rdv-eng@mail.ru</span>
						</div>
					</div>
					<div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
						<div class="icon d-flex justify-content-center align-items-center"><span class=""><i class="fa fa-phone"></i></span></div>
						<div class="text d-flex align-items-center">
							<span>8 (905) 716 - 04 - 06</span>
						</div>
					</div>
					<div class="col-md topper d-flex align-items-center align-items-stretch">
						<p class="mb-0 d-flex d-block">
							<a href="#" class="d-flex align-items-center justify-content-center">
								<span>
									<button class="btn btn-primary" style="background-color: transparent; border: 0; color: var(--main-button-color);" href="#section-appointment">Заказать звонок</button>
									<br>
									<span class="col-12" style="padding:0; white-space: nowrap;">
										<i class="fa fa-envelope col-6" style="text-align: center;"></i>
										<i class="fa fa-phone col-6" style="text-align: center;"></i>
									</span>
								</span>
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
	<div class="container d-flex align-items-center">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="oi oi-menu"></span> Menu
		</button>
		<!-- <form action="#" class="searchform order-lg-last">
			<div class="form-group d-flex">
				<input type="text" class="form-control pl-3" placeholder="Search">
				<button type="submit" placeholder="" class="form-control search"><span class="ion-ios-search"></span></button>
			</div>
		</form> -->
		<div class="collapse navbar-collapse" id="ftco-nav">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item <? echo $_SERVER['REDIRECT_URL'] == '' ? 'active' : ''; ?>">           <a href="/"           class="nav-link pl-0">Главная</a></li>
				<li class="nav-item <? echo $_SERVER['REDIRECT_URL'] == '/supply' ? 'active' : ''; ?>">    <a href="/supply"     class="nav-link">Поставка оборудования</a></li>
				<li class="nav-item <? echo $_SERVER['REDIRECT_URL'] == '/montage' ? 'active' : ''; ?>">   <a href="/montage"    class="nav-link">Монтаж</a></li>
				<li class="nav-item <? echo $_SERVER['REDIRECT_URL'] == '/projects' ? 'active' : ''; ?>">  <a href="/projects"   class="nav-link">Наши проекты</a></li>
				<li class="nav-item <? echo $_SERVER['REDIRECT_URL'] == '/certificat' ? 'active' : ''; ?>"><a href="/certificat" class="nav-link">Сертификаты</a></li>
				<li class="nav-item <? echo $_SERVER['REDIRECT_URL'] == '/contact' ? 'active' : ''; ?>">   <a href="/contact"    class="nav-link">Контакты</a></li>
			</ul>
		</div>
	</div>
</nav>