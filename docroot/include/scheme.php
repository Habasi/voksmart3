<section id="section-scheme" class="ftco-section testimony-section" style="/*background-image: url('/docroot/images/scheme-1.jpg'); background-repeat: no-repeat;
  background-position: top;
  background-size: cover;*/">

<div class="container">
    <!-- inspired by https://adobe.ly/3SZ9MC5 -->

<h1>Схема работ</h1>
<div class="container">

    <div class="itc-slider" data-slider="itc-slider" data-loop="false" data-autoplay="false">
    <div class="itc-slider-wrapper">
      <div class="itc-slider-items" id="scheme">
        <div class="itc-slider-item itc-slider-item-active">
          <!-- Контент 1 слайда -->
            <div style="width:100%; height: 100%;">
                <ol id="scheme2">
                  <li style="--accent-color: #FF6F00">
                    <div class="icon"><i class="fa-light fa-lightbulb-exclamation-on"></i></div>
                    <div class="title">Analysis</div>
                    <div class="descr">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</div>
                  </li>
                </ol>
            </div>
        </div>
        <div class="itc-slider-item itc-slider-item-active">
          <!-- Контент 2 слайда -->
          2
        </div>
        <div class="itc-slider-item itc-slider-item-active">
          <!-- Контент 3 слайда -->
          3
        </div>
        <div class="itc-slider-item">
          <!-- Контент 4 слайда -->
          4
        </div>
        <div class="itc-slider-item">
          <!-- Контент 5 слайда -->
          5
        </div>
      </div>
    </div>
    <button class="itc-slider-btn itc-slider-btn-prev itc-slider-btn-hide"></button>
    <button class="itc-slider-btn itc-slider-btn-next"></button>
  </div>
</div>

    <!--<div class="container">
        <div class="row ftco-animate">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="heading-section ftco-animate"> -->
                    <!-- <span class="subheading">Services</span> -->
                    <!-- <h2 class="mb-4">Схема работ</h2>
                </div>
                <div class="services-flow"> -->
                    <!-- <div class="services-2 p-4 d-flex ftco-animate">
                        <div class="icon">
                            <span class="flaticon-engineer"></span>
                        </div>
                        <div class="text">
                            <h3>Expert &amp; Professional</h3>
                            <p>Separated they live in. A small river named Duden flows</p>
                        </div>
                    </div>
                    <div class="services-2 p-4 d-flex ftco-animate">
                        <div class="icon">
                            <span class="flaticon-engineer-2"></span>
                        </div>
                        <div class="text">
                            <h3>24/7 Help Support</h3>
                            <p>Separated they live in. A small river named Duden flows</p>
                        </div>
                    </div> -->
                    <!--<ol id="scheme">
                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim sodales ut eu sem integer vitae justo. Aliquet sagittis id consectetur purus ut faucibus pulvinar elementum.</li>
                        <li>Nulla facilisi etiam dignissim diam quis enim. Dictum non consectetur a erat nam at lectus urna duis. Tristique magna sit amet purus gravida quis blandit turpis. Etiam non quam lacus suspendisse. Odio eu feugiat pretium nibh ipsum consequat nisl.</li>
                        <li>Sit amet purus gravida quis blandit turpis. Placerat orci nulla pellentesque dignissim. Sollicitudin tempor id eu nisl nunc mi ipsum. Pharetra et ultrices neque ornare aenean euismod elementum nisi quis. In nisl nisi scelerisque eu ultrices vitae auctor eu augue.</li>
                        <li>Mauris cursus mattis molestie a iaculis at erat pellentesque adipiscing. Scelerisque fermentum dui faucibus in ornare. Ornare arcu odio ut sem. Viverra nam libero justo laoreet sit amet. Amet justo donec enim diam. Vestibulum lectus mauris ultrices eros in cursus.</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>-->
    <script>

        let scheme = [
            {
                name: '<? apchi('scheme-name-1'); ?>',
                description: '<? apchi('scheme-description-1'); ?>',
                image: 'https://www.svgrepo.com/show/14925/phone-auricular-and-clock-delivery-symbol.svg',
                icon: 'fa-comments',
                color: '#696969',
            },
            {
                name: '<? apchi('scheme-name-2'); ?>',
                description: '<? apchi('scheme-description-2'); ?>',
                image: 'https://static.tildacdn.com/tild3035-6333-4231-b931-653562376531/119-1192879_construc.png',
                icon: 'fa-calculator',
                color: '#008DC2',
            },
            {
                name: '<? apchi('scheme-name-3'); ?>',
                description: '<? apchi('scheme-description-3'); ?>',
                image: '/docroot/images/scheme/1.png',
                icon: 'fa-handshake-simple',
                color: '#0B456A',
            },
            {
                name: '<? apchi('scheme-name-4'); ?>',
                description: '<? apchi('scheme-description-4'); ?>',
                image: '/docroot/images/scheme/4.png',
                icon: 'fa-people-carry-box',
                color: '#6A829A',
            },
            {
                name: '<? apchi('scheme-name-5'); ?>',
                description: '<? apchi('scheme-description-5'); ?>',
                image: 'https://grizly.club/uploads/posts/2023-02/1676158770_grizly-club-p-klipart-ventilyatsiya-41.png',
                icon: 'fa-wrench',
                color: '#696969',
            },
            {
                name: '<? apchi('scheme-name-6'); ?>',
                description: '<? apchi('scheme-description-6'); ?>',
                image: 'https://www.seekpng.com/png/full/462-4622316_asignacin-de-vehculos-controla-el-uso-de-tus.png',
                icon: 'fa-clipboard-check',
                color: '#008DC2',
            },
            {
                name: '<? apchi('scheme-name-7'); ?>',
                description: '<? apchi('scheme-description-7'); ?>',
                image: 'https://www.svgrepo.com/show/259353/certificate-contract.svg',
                icon: 'fa-shield',
                color: '#0B456A',
            },
        ];
        // document.querySelector('#scheme').innerHTML = scheme.map(function(item){
        //     return `
        //     <li>
        //         <h5>${item.name}</h5>
        //         <p>${item.description}</p>
        //     </li>
        //     `;
        // }).join('');
        
        document.querySelector('#scheme').innerHTML = scheme.map(function(item, i){
            return `
            <div class="itc-slider-item itc-slider-item-active">
          <!-- Контент 1 слайда -->
            <div style="width:100%; height: 100%;">
                <ol id="scheme">
                  ${'<li style="position: absolute; opacity: 0;"></li>'.repeat(i)}
                  <li style="--accent-color: ${item.color}">
                    <div class="icon"><i class="fa-light ${item.icon}"></i></div>
                    <div class="title">${item.name}</div>
                    <div class="descr">${item.description}</div>
                  </li>
                </ol>
            </div>
        </div>
            `;
        }).join('');

        // document.addEventListener('DOMContentLoaded', function(){
        //   let t = getJson(`/get-data`);
        //   console.log(t);
        // })
    </script>
</section>