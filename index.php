<?
/* configuration: 
    php-5.4, apache-2.2
    php-5.6 (-x64), apache 2.4 (-x64)
    php-7.0 - сломался сам openserver
*/

define("DIR", str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']));
require_once DIR . "/functions.php";
date_default_timezone_set('Europe/Moscow');
setlocale(LC_ALL, 'ru_RU', 'ru_RU.UTF-8', 'ru', 'russian');

session_start();

App::start('routes.php');

/* ------------ system: ------------ */
// echo file_get_contents(DIR . "/docroot/includes/"."form-product".".php");
/**
 * App (с автогенерацией .htaccess)
 * Запуск: App::start();
 */
class App {
    public static $htaccess = "
        AddDefaultCharset UTF-8 
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^(.*)$ index.php [L] \n";
    public static $routes = "<?
        Router::route('/', function(){ echo 'It Works'; });
        Router::route('404', function(){ echo 404; }); \n";
    public static $routes_puth = 'routes.php';

    public static function start($routes = null){
        date_default_timezone_set('Europe/Moscow');
        setlocale(LC_TIME, 'ru_RU');
        mb_internal_encoding('UTF-8');
        // .htaccess
        if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/.htaccess')){
            $file = fopen($_SERVER['DOCUMENT_ROOT'] . '/.htaccess', 'w');
            fwrite($file, self::$htaccess);		
            fclose($file);
        }
        // routes
        self::$routes_puth = $_SERVER['DOCUMENT_ROOT'] . '/' . (self::$routes_puth ?: self::$routes_puth);
        if(!file_exists(self::$routes_puth)){
            $file = fopen(self::$routes_puth, 'w');
            fwrite($file, self::$routes);		
            fclose($file);
        }
        require_once self::$routes_puth;
        Router::execute($_SERVER['REQUEST_URI']);
    }
}


/**
 * Автономный Router
 * Пример заполнения: Router::route('/test/(\w+)', function($wz){ echo $wz; });
 * Запуск: Router::execute($_SERVER['REQUEST_URI']);
 * 404: Router::route('404', function(){ echo 404; });
 */
class Router{
    public static $routes = [];
    
    public static function route($url, $callback){
        self::$routes['/^' . str_replace('/', '\/', $url) . '$/'] = $callback;
    }
    
    public static function execute($url){
        foreach(self::$routes as $pattern => $callback)
            if(preg_match($pattern, $url, $params))
                return call_user_func_array($callback, array_values($params));
        return @call_user_func_array(@self::$routes['/^404$/'], $_POST);
    }
}
